package com.example.ppagame;

import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by bala on 28.03.2018.
 */

public class ConnectToServer implements Runnable
{
    public Socket socket;
    @Override
    public void run() {
        try
        {
            socket=new Socket("192.168.0.103",7000);
            MainActivity.objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            MainActivity.objectInputStream = new ObjectInputStream(socket.getInputStream());
        }
        catch (UnknownHostException e1)
        {
            //MainActivity.feedback_text.setText("Can't connect to server!UnknownHostException: "+e1.getMessage());
        }
        catch (IOException e2)
        {
          //  MainActivity.feedback_text.setText("Can't connect to server!IOException: "+e2.getMessage());
        }
        catch (SecurityException e3)
        {
           // MainActivity.feedback_text.setText("Can't connect to server!SecurityException: "+e3.getMessage());
        }
        catch (IllegalArgumentException e4)
        {
           // MainActivity.feedback_text.setText("Can't connect to server!IllegalArgumentException: "+e4.getMessage());
        }
    }
}
