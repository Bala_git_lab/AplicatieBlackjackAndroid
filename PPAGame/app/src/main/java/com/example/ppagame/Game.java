package com.example.ppagame;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ThreadFactory;

/**
 * Created by bala on 07.04.2018.
 */

public class Game implements Runnable
{
    public ObjectOutputStream objectOutputStream;
    public ObjectInputStream objectInputStream;
    public Thread sendMessagesThread;
    public SendMessages sendMessagesRunnable;
    public Context context;
    public volatile ArrayList<Object> date;
    public boolean game_finished;
    public volatile String command;
    public String data_from_enemy;
    public volatile String message;
    public String carte;
    public int nr_carti;
    public int player_cards_sum1;
    public int player_cards_sum2;
    public String carte_adversar;
    public int opponent_cards_sum1;
    public int opponent_cards_sum2;
    public int player_best_sum;
    public int opponent_best_sum;
    public Thread wait_for_data;
    public Thread animatie_primire_mesaj;
    public boolean exit_game=false;
    public Game(ObjectOutputStream objectOutputStream, ObjectInputStream objectInputStream, Context context)
    {
        this.objectOutputStream=objectOutputStream;
        this.objectInputStream=objectInputStream;
        this.context=context;
        date=new ArrayList<>();
        game_finished=false;
        this.context=context;
    }
    @Override
    public void run()
    {
        try
        {
                while (game_finished == false) {

                    //aici primesc cartile si alte date de inceput
                    date = (ArrayList<Object>) objectInputStream.readObject();//am primit cele 4 carti de la server, cine incepe primul si suma cartilor
                    Game_Activity.right_card = (String) date.get(0);
                    Game_Activity.left_card = (String) date.get(1);
                    Game_Activity.enemy_right_card = (String) date.get(2);
                    Game_Activity.enemy_left_card = (String) date.get(3);
                    String info;
                    info = (String) date.get(4);
                    if (info.equals("I start first")) {
                        Game_Activity.I_start_first = true;
                        Game_Activity.my_turn = true;
                        Log.d("Carti-Thread: ", "right_card: " + Game_Activity.right_card + " left_card: " + Game_Activity.left_card);
                    } else {
                        Game_Activity.I_start_first = false;
                        Game_Activity.my_turn = false;
                    }
                    Game_Activity.my_cards_sum1=(Integer)date.get(5);
                    Game_Activity.enemy_cards_sum1=(Integer)date.get(6);
                    player_cards_sum1=(Integer)date.get(5);
                    opponent_cards_sum1=(Integer)date.get(6);
                    if(Game_Activity.right_card.equals("as_inima-neagra")||Game_Activity.right_card.equals("as_inima-rosie")||Game_Activity.right_card.equals("as_trefla")||Game_Activity.right_card.equals("as_romb")||Game_Activity.left_card.equals("as_inima-neagra")||Game_Activity.left_card.equals("as_inima-rosie")||Game_Activity.left_card.equals("as_trefla")||Game_Activity.left_card.equals("as_romb"))
                    {
                        player_cards_sum2=player_cards_sum1-1+11;
                        Game_Activity.my_cards_sum2=player_cards_sum2;
                        Log.d("Game:", "AM UN AS");
                    }
                    else
                    {
                        player_cards_sum2=player_cards_sum1;
                        Game_Activity.my_cards_sum2=player_cards_sum2;
                        Log.d("Game:", "NU AM UN AS");
                    }
                    if(Game_Activity.enemy_right_card.equals("as_inima-neagra")||Game_Activity.enemy_right_card.equals("as_inima-rosie")||Game_Activity.enemy_right_card.equals("as_trefla")||Game_Activity.enemy_right_card.equals("as_romb")||Game_Activity.enemy_left_card.equals("as_inima-neagra")||Game_Activity.enemy_left_card.equals("as_inima-rosie")||Game_Activity.enemy_left_card.equals("as_trefla")||Game_Activity.enemy_left_card.equals("as_romb"))
                    {
                        opponent_cards_sum2=opponent_cards_sum1-1+11;
                        Game_Activity.enemy_cards_sum2=opponent_cards_sum2;
                        Log.d("Game:", "ARE UN AS");
                    }
                    else
                    {
                        opponent_cards_sum2=opponent_cards_sum1;
                        Game_Activity.enemy_cards_sum2=opponent_cards_sum2;
                        Log.d("Game:", "NU ARE UN AS");
                    }


                    Log.d("Thread: ", "Suma cartilor: "+Game_Activity.my_cards_sum1);

                    Thread.sleep(1000);

                    Message mesaj = Message.obtain();
                    mesaj.arg1 = 0;
                    Game_Activity.handler.sendMessage(mesaj);//trimit un mesaj catre threadul cu UI ca am primit cartile si vreau sa schinb cartea stanga

                    Thread.sleep(500);

                    mesaj = Message.obtain();
                    mesaj.arg1 = 1;
                    Game_Activity.handler.sendMessage(mesaj);//vreasu sa schimb cartea dreapta

                    Thread.sleep(500);

                    mesaj=Message.obtain();//aici afisez suma
                    mesaj.arg1=5;
                    Game_Activity.handler.sendMessage(mesaj);

                    //aici se termina primire cartilor si a datelor de inceput

                    //aici incep mana
                    if (Game_Activity.I_start_first == true)
                    {
                        nr_carti=0;
                        wait_for_data=new Thread(new WaitForMessages(objectInputStream,Thread.currentThread(),this));//asta am adaugat azi
                        wait_for_data.start();//asta am adaugat azi

                        while (Game_Activity.my_turn == true)//cand e randul meu si incep primul
                        {

                            try
                            {
                                Thread.sleep(10000000);
                            }
                            catch (InterruptedException a)
                            {
                                Log.d("GameThread:", "Thread was interrupted");//threadul se trezeste atunci cand apas pe butonul de hit sau de stay
                            }
                            Log.d("GameThread: ", "am continuat");//am asteptat inputul playerului


                            if(command.equals("hit"))
                            {
                                nr_carti++;
                                date = new ArrayList<>();
                                date.add("hit");
                                objectOutputStream.writeObject(date);


                                try//astept sa primesc cartea de la server(am scris azi)
                                {
                                    Thread.sleep(999999999);
                                }
                                catch(InterruptedException ex)
                                {
                                    ex.printStackTrace();
                                }
                                //date=(ArrayList<Object>) objectInputStream.readObject();//primesc o carte de la Server sau un mesaj de la server (care e trimis de adversar)

                                carte=(String)date.get(1);
                                int card_value;
                                card_value=(Integer)date.get(2)-player_cards_sum1;//suma finala-suma initiala
                                player_cards_sum1=(Integer)date.get(2);
                                Log.d("Thread: ", "Carte: "+carte);
                                //trimit mesaj catre UI thread ca am primit date de la server si modific o carte
                                Game_Activity.my_card=carte;
                                Game_Activity.my_cards_sum1=player_cards_sum1;
                                if(carte.equals("as_inima-neagra")||carte.equals("as_inima-rosie")||carte.equals("as_trefla")||carte.equals("as_romb"))
                                {
                                    player_cards_sum2=player_cards_sum1-1+11;
                                    Game_Activity.my_cards_sum2=player_cards_sum2;
                                }
                                else
                                {
                                    player_cards_sum2+=card_value;
                                    Game_Activity.my_cards_sum2=player_cards_sum2;
                                }
                                mesaj=Message.obtain();
                                mesaj.arg1=2;
                                Game_Activity.handler.sendMessage(mesaj);//am trimis mesajul catre UI thread ca sa afiseze cartea primita de player si sa dea update la suma
                                if (player_cards_sum1==21||player_cards_sum2==21)
                                {
                                    //blackjack
                                    Thread.sleep(500);
                                    mesaj=Message.obtain();
                                    mesaj.arg1=6;
                                    Game_Activity.handler.sendMessage(mesaj);//ii zic threadului de UI ca playerul a facut blackjack
                                    Game_Activity.my_turn=false;//s-a terminat tura
                                }
                                else
                                {
                                    if (player_cards_sum1 > 21 && player_cards_sum2 > 21)
                                    {
                                        //busted
                                        Thread.sleep(500);
                                        mesaj = Message.obtain();
                                        mesaj.arg1 = 7;
                                        Game_Activity.handler.sendMessage(mesaj);//ii zic threadului de UI ca playerul a avut suma prea mare
                                        Game_Activity.my_turn = false;//s-a terminat tura
                                    }

                                }
                            }
                            if(command.equals("stay"))
                            {
                                date = new ArrayList<>();
                                String stay="stay";
                                date.add(stay);
                                objectOutputStream.writeObject(date);
                                mesaj=Message.obtain();
                                mesaj.arg1=8;
                                Game_Activity.handler.sendMessage(mesaj);//ii zic threadului de UI ca playerul a terminat tura
                                Game_Activity.my_turn=false;//s-a terminat tura
                            }
                            if(command.equals("trimite mesaj"))
                            {
                                date=new ArrayList<>();
                                date.add("trimite mesaj");
                                date.add(message);
                                objectOutputStream.writeObject(date);
                            }
                            if(command.equals("exit"))
                            {
                                date=new ArrayList<>();
                                date.add("exit");
                                objectOutputStream.writeObject(date);//trimit catre server faptul ca vreau sa ies din joc
                                exit_game=true;
                                Log.d("E randul meu: ", "Am apasat exit si ies din threadul cu jocul");
                                break;
                            }
                        }
                        if(exit_game)
                            break;




                        Thread.sleep(500);
                        //cand nu e randul meu dar incep primul: astept cartile adversarului sau mesajele si trimit mesaje adversarului
                        nr_carti=0;
                        mesaj=Message.obtain();
                        mesaj.arg1=9;
                        Game_Activity.handler.sendMessage(mesaj);//intorc cartea adversarului pe dos ca sa o vad si scriu suma cartilor adversarului
                        sendMessagesRunnable=new SendMessages(objectOutputStream,Thread.currentThread());
                        sendMessagesThread=new Thread(sendMessagesRunnable);
                        sendMessagesThread.start();
                        //am pornit threadul care trimite mesaje(threadul asteapta ca butonul de send sa fie apasat in UIThread si trimite mesajul
                        while (Game_Activity.my_turn==false)//trebuie sa pot sa trimit mesaje si sa pot primi carti si mesaje
                        {
                            date=new ArrayList<>();
                            date=(ArrayList<Object>)objectInputStream.readObject();//primesc date de la adversar
                            data_from_enemy=(String)date.get(0);
                            if (data_from_enemy.equals("Hand has ended")==false)//daca stringul trimis nu e egal cu hand has ended inseamna ca e sigur o carte
                            {
                                if(data_from_enemy.equals("carte")) {//nu are sens sa astept mesajul si cartea in acelasi timp pt ca adversarul nu va putea trimite mesajul in acelasi timp cu cartea
                                    nr_carti++;
                                    int card_value;
                                    card_value=(Integer) date.get(2)-opponent_cards_sum1;//suma finala-suma initiala
                                    carte_adversar=(String)date.get(1);
                                    opponent_cards_sum1=(Integer) date.get(2);
                                    Game_Activity.opponent_card = carte_adversar;
                                    Game_Activity.enemy_cards_sum1 = opponent_cards_sum1;
                                    if(carte_adversar.equals("as_inima-neagra")||carte_adversar.equals("as_inima-rosie")||carte_adversar.equals("as_trefla")||carte_adversar.equals("as_romb"))
                                    {
                                        opponent_cards_sum2=opponent_cards_sum1-1+11;
                                        Game_Activity.enemy_cards_sum2=opponent_cards_sum2;
                                    }
                                    else
                                    {
                                        opponent_cards_sum2+=card_value;
                                        Game_Activity.enemy_cards_sum2=opponent_cards_sum2;
                                    }
                                    mesaj = Message.obtain();
                                    mesaj.arg1 = 3;
                                    Game_Activity.handler.sendMessage(mesaj);//am trimis faptul ca am primit o carte primita de playerul 2 si suma cartilor playerului 2
                                }
                                else
                                {
                                    if(data_from_enemy.equals("primeste mesaj"))//e un mesaj
                                    {
                                        String mesaj_primit;
                                        mesaj_primit = (String) date.get(1);
                                        Game_Activity.mesaj_primit = mesaj_primit;
                                        Message message = Message.obtain();
                                        message.arg1 = 14;
                                        Game_Activity.handler.sendMessage(message);
                                        //vreau sa maresc bubble-ul cu mesajul si sa il micsorez putin(sa am un efect)
                                        animatie_primire_mesaj = new Thread(new EfectSpecialMesaj());
                                        animatie_primire_mesaj.start();
                                    }
                                    else//adversarul vrea sa iasa din joc
                                    {
                                        Log.d("nu e randul meu:", "adversarul a iesit din joc: ");
                                        Intent intent = new Intent(context, GameMenuActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        context.startActivity(intent);
                                        exit_game=true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                Game_Activity.my_turn=true;//s-a terminat tura
                                if(Game_Activity.enemy_cards_sum1==21||Game_Activity.enemy_cards_sum2==21)
                                {
                                    //BlackJack
                                    Thread.sleep(100);
                                    mesaj=Message.obtain();
                                    mesaj.arg1=10;
                                    Game_Activity.handler.sendMessage(mesaj);//ii zic playerului 1 ca playerul 2 a facut BlackJack
                                }
                                else
                                {
                                    if(Game_Activity.enemy_cards_sum1>21&&Game_Activity.enemy_cards_sum2>21)
                                    {
                                        //Busted
                                        Thread.sleep(100);
                                        mesaj=Message.obtain();
                                        mesaj.arg1=11;
                                        Game_Activity.handler.sendMessage(mesaj);//ii zic playerului 1 ca playerul 2 a avut prea multe carti
                                    }
                                }
                                //selectez cele mai bune sume ale jucatorilor
                                if(Game_Activity.my_cards_sum1==21||Game_Activity.my_cards_sum2==21)
                                {
                                    player_best_sum=21;
                                }
                                else
                                {
                                    if(Game_Activity.my_cards_sum1>21&&Game_Activity.my_cards_sum2>21)
                                    {
                                        player_best_sum=Game_Activity.my_cards_sum1;
                                    }
                                    else
                                    {
                                        if(Game_Activity.my_cards_sum1<21&&Game_Activity.my_cards_sum2<21)
                                        {
                                            if(Game_Activity.my_cards_sum1>Game_Activity.my_cards_sum2)
                                            {
                                                player_best_sum=Game_Activity.my_cards_sum1;
                                            }
                                            else
                                            {
                                                player_best_sum=Game_Activity.my_cards_sum2;
                                            }
                                        }
                                        else
                                        {
                                            if(Game_Activity.my_cards_sum1>21)
                                                player_best_sum=Game_Activity.my_cards_sum2;
                                            else
                                                player_best_sum=Game_Activity.my_cards_sum1;
                                        }
                                    }
                                }
                                if(Game_Activity.enemy_cards_sum1==21||Game_Activity.enemy_cards_sum2==21)
                                {
                                    opponent_best_sum=21;
                                }
                                else
                                {
                                    if(Game_Activity.enemy_cards_sum1>21&&Game_Activity.enemy_cards_sum2>21)
                                    {
                                        opponent_best_sum=Game_Activity.enemy_cards_sum1;
                                    }
                                    else
                                    {
                                        if(Game_Activity.enemy_cards_sum1<21&&Game_Activity.enemy_cards_sum2<21)
                                        {
                                            if(Game_Activity.enemy_cards_sum1>Game_Activity.enemy_cards_sum2)
                                            {
                                                opponent_best_sum=Game_Activity.enemy_cards_sum1;
                                            }
                                            else
                                            {
                                                opponent_best_sum=Game_Activity.enemy_cards_sum2;
                                            }
                                        }
                                        else
                                        {
                                            if(Game_Activity.enemy_cards_sum1>21)
                                                opponent_best_sum=Game_Activity.enemy_cards_sum2;
                                            else
                                                opponent_best_sum=Game_Activity.enemy_cards_sum1;
                                        }
                                    }
                                }
                                //verific cine a castigat
                                if((player_best_sum>opponent_best_sum)&&(player_best_sum<21&&opponent_best_sum<21))
                                {
                                    //am castigat
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=0;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }
                                if(opponent_best_sum>21)
                                {
                                    //am castigat
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=0;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }
                                if(player_best_sum==21&&opponent_best_sum<21)
                                {
                                    //am castigat
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=0;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }
                                if(player_best_sum==21&&opponent_best_sum==21) {
                                    //egalitate
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=1;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }
                                if((player_best_sum==opponent_best_sum)&&(player_best_sum<21&&opponent_best_sum<21)) {
                                    //egalitate
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=1;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }
                                if((player_best_sum<opponent_best_sum)&&(player_best_sum<21&&opponent_best_sum<21))
                                {
                                    //am pierdut
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=2;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }
                                if(player_best_sum<21&&opponent_best_sum==21)
                                {
                                    //am pierdut
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=2;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }
                                if(player_best_sum>21)
                                {
                                    //am pierdut
                                    Message mesaj_win=Message.obtain();
                                    mesaj_win.arg1=2;
                                    Game_Activity.handler2.sendMessage(mesaj_win);
                                }

                                Thread.sleep(200);
                                //fac aefectul acela de marire+micsorare al textului
                                int total_animatie=3000;
                                int timer=0;
                                boolean mareste=true;
                                boolean micsoreaza=false;
                                while(total_animatie>0)
                                {

                                    if(mareste==true)//maresc textul
                                    {
                                        total_animatie=total_animatie-100;
                                        timer=timer+100;
                                        Thread.sleep(100);
                                        Message mesaj2=Message.obtain();
                                        mesaj2.arg1=3;
                                        mesaj2.arg2=0;//marire
                                        Game_Activity.handler2.sendMessage(mesaj2);
                                        if(timer==1500)
                                        {
                                            mareste=false;
                                            micsoreaza=true;
                                        }
                                    }
                                    if(micsoreaza==true)
                                    {
                                        total_animatie=total_animatie-100;
                                        timer=timer+100;
                                        Thread.sleep(100);
                                        Message mesaj2=Message.obtain();
                                        mesaj2.arg1=3;
                                        mesaj2.arg2=1;//micsorez textul
                                        Game_Activity.handler2.sendMessage(mesaj2);
                                        if(timer==1500)
                                        {
                                            micsoreaza=false;
                                            mareste=true;
                                        }
                                    }

                                }
                                //reinitializez datele
                                Thread.sleep(100);
                                mesaj=Message.obtain();
                                mesaj.arg1=12;
                                Game_Activity.handler.sendMessage(mesaj);
                                //daca eu am inceput primul urmatoarea tura va incepe adversarul asa ca imi acopar cartea dreapta si sterg toate cartile deja puse pe masa
                            }
                        }
                        //opresc threadul care trimite mesajele
                        sendMessagesRunnable.command="finish thread";
                        sendMessagesThread.interrupt();
                        if(exit_game==true)
                            break;
                        Thread.sleep(200);
                    }
                    else//NU INCEP PRIMUL
                    {
                        Log.d("GameThread: ", "nu eu incep primul");
                        Game_Activity.my_turn=false;
                        nr_carti=0;
                        sendMessagesRunnable=new SendMessages(objectOutputStream,Thread.currentThread());
                        sendMessagesThread=new Thread(sendMessagesRunnable);
                        sendMessagesThread.start();
                        //am pornit threadul care trimite mesaje(threadul asteapta ca butonul de send sa fie apasat in UIThread si trimite mesajul
                        while (Game_Activity.my_turn==false)
                        {
                            date=new ArrayList<>();
                            date=(ArrayList<Object>)objectInputStream.readObject();
                            data_from_enemy=(String)date.get(0);
                            Log.d("Adversar", "Date: "+data_from_enemy);
                            if (data_from_enemy.equals("My turn")==false)//daca stringul trimis nu e egal cu my turn inseamna ca e sigur o carte
                            {
                                if(data_from_enemy.equals("carte")) {
                                    nr_carti++;
                                    int card_value;
                                    card_value=(Integer) date.get(2)-opponent_cards_sum1;//suma initiala-suma finala
                                    carte_adversar=(String)date.get(1);
                                    opponent_cards_sum1=(Integer) date.get(2);
                                    Log.d("Adversar", "Nr carti: " + nr_carti);
                                    Game_Activity.opponent_card = carte_adversar;
                                    Game_Activity.enemy_cards_sum1 = opponent_cards_sum1;
                                    if(carte_adversar.equals("as_inima-neagra")||carte_adversar.equals("as_inima-rosie")||carte_adversar.equals("as_trefla")||carte_adversar.equals("as_romb"))
                                    {
                                        opponent_cards_sum2=opponent_cards_sum1-1+11;
                                        Game_Activity.enemy_cards_sum2=opponent_cards_sum2;
                                    }
                                    else
                                    {
                                        opponent_cards_sum2+=card_value;
                                        Game_Activity.enemy_cards_sum2=opponent_cards_sum2;
                                    }
                                    mesaj = Message.obtain();
                                    mesaj.arg1 = 3;
                                    Game_Activity.handler.sendMessage(mesaj);//am trimis faptul ca am primit o carte primita de adversar si suma cartilor adversarului
                                }
                                else
                                {
                                    if(data_from_enemy.equals("primeste mesaj"))//e un mesaj
                                    {
                                        String mesaj_primit;
                                        mesaj_primit = (String) date.get(1);
                                        Game_Activity.mesaj_primit = mesaj_primit;
                                        Message message = Message.obtain();
                                        message.arg1 = 14;
                                        Game_Activity.handler.sendMessage(message);
                                        //vreau sa maresc bubble-ul cu mesajul si sa il micsorez putin(sa am un efect)
                                        animatie_primire_mesaj = new Thread(new EfectSpecialMesaj());
                                        animatie_primire_mesaj.start();
                                    }
                                    else//adversarul vrea sa iasa din joc
                                    {
                                        Log.d("nu e randul meu:", "adversarul a iesit din joc: ");
                                        Intent intent = new Intent(context, GameMenuActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        context.startActivity(intent);
                                        exit_game=true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                Log.d("E randul meu:", " si vreau sa verific daca adversarul are prea multe carti sau nu");
                                Game_Activity.my_turn=true;//e randul meu
                                if(Game_Activity.enemy_cards_sum1==21||Game_Activity.enemy_cards_sum2==21)
                                {
                                    //BlackJack
                                    Thread.sleep(100);
                                    mesaj=Message.obtain();
                                    mesaj.arg1=10;
                                    Game_Activity.handler.sendMessage(mesaj);//ii zic playerului 1 ca playerul 2 a facut BlackJack
                                }
                                else
                                {
                                    if(Game_Activity.enemy_cards_sum1>21&&Game_Activity.enemy_cards_sum2>21)
                                    {
                                        //Busted
                                        Thread.sleep(100);
                                        mesaj=Message.obtain();
                                        mesaj.arg1=11;
                                        Game_Activity.handler.sendMessage(mesaj);//ii zic playerului 1 ca playerul 2 a avut prea multe carti
                                    }
                                }
                            }
                        }

                        //opresc threadul care trimite mesajele
                        sendMessagesRunnable.command="finish thread";
                        sendMessagesThread.interrupt();

                        if(exit_game)
                            break;

                        Thread.sleep(500);
                        //cand e randul meu si nu incep primul
                        nr_carti=0;
                        mesaj=Message.obtain();
                        mesaj.arg1=4;
                        Game_Activity.handler.sendMessage(mesaj);//ii zic playerului ca e randul lui si afisez butoanele intorc arat cartea din dreapta si arat suma initiala a cartilor
                        wait_for_data=new Thread(new WaitForMessages(objectInputStream,Thread.currentThread(),this));//asta am adaugat azi
                        wait_for_data.start();//asta am adaugat azi

                        while (Game_Activity.my_turn == true)//e randul meu si nu incep primul
                        {
                            try
                            {
                                Thread.sleep(10000000);
                            }
                            catch (InterruptedException a)
                            {
                                Log.d("GameThread:", "Thread was interrupted");
                            }
                            Log.d("GameThread: ", "am continuat");//am asteptat inputul playerului


                            if(command.equals("hit"))
                            {
                                nr_carti++;
                                date = new ArrayList<>();
                                date.add("hit");
                                objectOutputStream.writeObject(date);


                                try//astept sa primesc cartea de la server(am scris azi)
                                {
                                    Thread.sleep(999999999);
                                }
                                catch(InterruptedException ex)
                                {
                                    ex.printStackTrace();
                                }
                               // date=(ArrayList<Object>) objectInputStream.readObject();


                                carte=(String)date.get(1);
                                int card_value;
                                card_value=(Integer)date.get(2)-player_cards_sum1;//suma finala-suma initiala
                                player_cards_sum1=(Integer)date.get(2);
                                Log.d("Thread: ", "Carte: "+carte);
                                //trimit mesaj catre UI thread ca am primit date de la server si modific o carte
                                Game_Activity.my_card=carte;
                                Game_Activity.my_cards_sum1=player_cards_sum1;
                                if(carte.equals("as_inima-neagra")||carte.equals("as_inima-rosie")||carte.equals("as_trefla")||carte.equals("as_romb"))
                                {
                                    player_cards_sum2=player_cards_sum1-1+11;
                                    Game_Activity.my_cards_sum2=player_cards_sum2;
                                }
                                else
                                {
                                    player_cards_sum2=player_cards_sum2+card_value;
                                    Game_Activity.my_cards_sum2=player_cards_sum2;
                                }
                                mesaj=Message.obtain();
                                mesaj.arg1=2;
                                Game_Activity.handler.sendMessage(mesaj);//am trimis mesajul catre UI thread ca sa afiseze cartea primita de player

                                if (player_cards_sum1==21||player_cards_sum2==21)
                                {
                                    //blackjack
                                    Thread.sleep(500);
                                    mesaj=Message.obtain();
                                    mesaj.arg1=6;
                                    Game_Activity.handler.sendMessage(mesaj);//ii zic threadului de UI ca playerul a facut blackjack
                                    Game_Activity.my_turn=false;//s-a terminat tura
                                }
                                else
                                {
                                    if (player_cards_sum1 > 21 && player_cards_sum2 > 21)
                                    {
                                        //busted
                                        Thread.sleep(500);
                                        mesaj = Message.obtain();
                                        mesaj.arg1 = 7;
                                        Game_Activity.handler.sendMessage(mesaj);//ii zic threadului de UI ca playerul a avut suma prea mare
                                        Game_Activity.my_turn = false;//s-a terminat tura
                                    }

                                }
                            }
                            if(command.equals("stay"))
                            {
                                date = new ArrayList<>();
                                String stay="stay";
                                date.add(stay);
                                objectOutputStream.writeObject(date);
                                Game_Activity.my_turn=false;
                            }
                            if(command.equals("trimite mesaj"))
                            {
                                date=new ArrayList<>();
                                date.add("trimite mesaj");
                                date.add(message);
                                objectOutputStream.writeObject(date);
                            }
                            if(command.equals("exit"))
                            {
                                date=new ArrayList<>();
                                date.add("exit");
                                objectOutputStream.writeObject(date);//trimit catre server faptul ca vreau sa ies din joc
                                exit_game=true;
                                break;
                            }
                        }
                        if(exit_game==true)
                            break;
                        //selectez cele mai bune sume ale jucatorilor
                        if(Game_Activity.my_cards_sum1==21||Game_Activity.my_cards_sum2==21)
                        {
                            player_best_sum=21;
                        }
                        else
                        {
                            if(Game_Activity.my_cards_sum1>21&&Game_Activity.my_cards_sum2>21)
                            {
                                player_best_sum=Game_Activity.my_cards_sum1;
                            }
                            else
                            {
                                if(Game_Activity.my_cards_sum1<21&&Game_Activity.my_cards_sum2<21)
                                {
                                    if(Game_Activity.my_cards_sum1>Game_Activity.my_cards_sum2)
                                    {
                                        player_best_sum=Game_Activity.my_cards_sum1;
                                    }
                                    else
                                    {
                                        player_best_sum=Game_Activity.my_cards_sum2;
                                    }
                                }
                                else
                                {
                                    if(Game_Activity.my_cards_sum1>21)
                                        player_best_sum=Game_Activity.my_cards_sum2;
                                    else
                                        player_best_sum=Game_Activity.my_cards_sum1;
                                }
                            }
                        }
                        if(Game_Activity.enemy_cards_sum1==21||Game_Activity.enemy_cards_sum2==21)
                        {
                            opponent_best_sum=21;
                        }
                        else
                        {
                            if(Game_Activity.enemy_cards_sum1>21&&Game_Activity.enemy_cards_sum2>21)
                            {
                                opponent_best_sum=Game_Activity.enemy_cards_sum1;
                            }
                            else
                            {
                                if(Game_Activity.enemy_cards_sum1<21&&Game_Activity.enemy_cards_sum2<21)
                                {
                                    if(Game_Activity.enemy_cards_sum1>Game_Activity.enemy_cards_sum2)
                                    {
                                        opponent_best_sum=Game_Activity.enemy_cards_sum1;
                                    }
                                    else
                                    {
                                        opponent_best_sum=Game_Activity.enemy_cards_sum2;
                                    }
                                }
                                else
                                {
                                    if(Game_Activity.enemy_cards_sum1>21)
                                        opponent_best_sum=Game_Activity.enemy_cards_sum2;
                                    else
                                        opponent_best_sum=Game_Activity.enemy_cards_sum1;
                                }
                            }
                        }


                        //verific cine a castigat
                        if((player_best_sum>opponent_best_sum)&&(player_best_sum<21&&opponent_best_sum<21))
                        {
                            //am castigat
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=0;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }
                        if(opponent_best_sum>21)
                        {
                            //am castigat
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=0;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }
                        if(player_best_sum==21&&opponent_best_sum<21)
                        {
                            //am castigat
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=0;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }
                        if(player_best_sum==21&&opponent_best_sum==21) {
                            //egalitate
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=1;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }
                        if((player_best_sum==opponent_best_sum)&&(player_best_sum<21&&opponent_best_sum<21)) {
                            //egalitate
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=1;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }
                        if((player_best_sum<opponent_best_sum)&&(player_best_sum<21&&opponent_best_sum<21))
                        {
                            //am pierdut
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=2;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }
                        if(player_best_sum<21&&opponent_best_sum==21)
                        {
                            //am pierdut
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=2;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }
                        if(player_best_sum>21)
                        {
                            //am pierdut
                            Message mesaj_win=Message.obtain();
                            mesaj_win.arg1=2;
                            Game_Activity.handler2.sendMessage(mesaj_win);
                        }



                        Thread.sleep(200);
                        //fac aefectul acela de marire+micsorare al textului
                        int total_animatie=3000;
                        int timer=0;
                        boolean mareste=true;
                        boolean micsoreaza=false;
                        while(total_animatie>0)
                        {

                            if(mareste==true)//maresc textul
                            {
                                total_animatie=total_animatie-100;
                                timer=timer+100;
                                Thread.sleep(100);
                                Message mesaj2=Message.obtain();
                                mesaj2.arg1=3;
                                mesaj2.arg2=0;//marire
                                Game_Activity.handler2.sendMessage(mesaj2);
                                if(timer==1500)
                                {
                                    mareste=false;
                                    micsoreaza=true;
                                }
                            }
                            if(micsoreaza==true)
                            {
                                total_animatie=total_animatie-100;
                                timer=timer+100;
                                Thread.sleep(100);
                                Message mesaj2=Message.obtain();
                                mesaj2.arg1=3;
                                mesaj2.arg2=1;//micsorez textul
                                Game_Activity.handler2.sendMessage(mesaj2);
                                if(timer==1500)
                                {
                                    micsoreaza=false;
                                    mareste=true;
                                }
                            }

                        }







                        //reinitializez datele
                        Thread.sleep(100);
                        mesaj=Message.obtain();
                        mesaj.arg1=13;
                        Game_Activity.handler.sendMessage(mesaj);
                        //daca eu NU am inceput primul urmatoarea tura voi incepe asa ca ii acopar cartea dreapta si sterg toate cartile deja puse pe masa
                    }
                }

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
