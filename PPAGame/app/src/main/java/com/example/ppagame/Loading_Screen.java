package com.example.ppagame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

public class Loading_Screen extends AppCompatActivity {
    public static volatile Context loading_screen_context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_loading__screen);
        loading_screen_context=(Context)this;
        Log.d("Loading screen", "Astept sa ma joc");
    }
}
