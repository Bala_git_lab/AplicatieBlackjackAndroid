package com.example.ppagame;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by bala on 06.04.2018.
 */

public class Search_for_game implements Runnable
{
    public ObjectOutputStream objectOutputStream;
    public ObjectInputStream objectInputStream;
    public ArrayList<Object> date;
    public Context context;
    public Search_for_game(ObjectOutputStream objectOutputStream, ObjectInputStream objectInputStream, ArrayList<Object> date,Context context)
    {
        this.objectOutputStream=objectOutputStream;
        this.objectInputStream=objectInputStream;
        this.date=date;
        this.context=context;
    }
    @Override
    public void run()
    {
        try
        {
            objectOutputStream.writeObject(date);//trimit datele userului care vrea sa joace catre Server
            Log.d("Meniu", "Am trimis datele userului");
            date=(ArrayList<Object>)objectInputStream.readObject();//primesc userul la care trebuie sa ma conectez
            GameMenuActivity.opponent_data=(UserData) date.get(0);
            Intent intent=new Intent(context,Game_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);//pornesc activitatea care imi leaga doi jucatori si pot sa se joace
        }
        catch (IOException e)
        {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}
