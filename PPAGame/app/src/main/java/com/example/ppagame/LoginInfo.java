package com.example.ppagame;
import java.io.Serializable;
public class LoginInfo implements Serializable
{
    public boolean sign_in;
    public boolean sign_up;
    public LoginInfo(boolean sign_in,boolean sign_up)
    {
        this.sign_in=sign_in;
        this.sign_up=sign_up;
    }
}
