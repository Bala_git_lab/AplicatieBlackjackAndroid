package com.example.ppagame;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class GameMenuActivity extends AppCompatActivity {


    public Context game_menu_context;
    public TextView title_text_view;
    public TextView username_text_view;
    public Button multiplayer_button;
    public Button single_player_button;
    public Button store_button;
    public Button settings_button;
    public static volatile ArrayList<Object> date;
    public static volatile UserData userData;
    public static volatile UserData opponent_data;
    public Thread search_for_game;
    public static volatile boolean finished_thread=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_game_menu);
        initUI();

        game_menu_context=(Context)this;
        userData=MainActivity.userData;
        date=new ArrayList<>();


            multiplayer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userData.wants_to_play=true;
                //loading screen
                Intent intent=new Intent(game_menu_context,Loading_Screen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                date.add(userData);
                search_for_game=new Thread(new Search_for_game(MainActivity.objectOutputStream,MainActivity.objectInputStream,date,game_menu_context));
                search_for_game.start();

            }
        });
        single_player_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(game_menu_context,SinglePlayerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }
    public void initUI()
    {

        title_text_view=(TextView)findViewById(R.id.Title_Menu);
        title_text_view.setTextSize(40);
        title_text_view.setTextColor(android.graphics.Color.argb(255,255,188,0));

        username_text_view=(TextView)findViewById(R.id.username_menu_text_view);
        username_text_view.setTextSize(20);
        username_text_view.setTextColor(Color.BLACK);
        username_text_view.setText(MainActivity.userData.username);

        multiplayer_button=(Button)findViewById(R.id.multiplayer_button);

        single_player_button=(Button)findViewById(R.id.single_player_button);

        store_button=(Button)findViewById(R.id.store_button);

        settings_button=(Button)findViewById(R.id.settings_button);
    }
}
