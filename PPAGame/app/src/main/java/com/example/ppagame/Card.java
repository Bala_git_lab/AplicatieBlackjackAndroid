package com.example.ppagame;

/**
 * Created by bala on 13.05.2018.
 */

public class Card {
    private int image;
    private int value;


    public Card(int image,int value){
        this.value=value;
        this.image=image;
    }



    public int getImage(){
        return this.image;
    }

    public int getValue(){
        return this.value;
    }

}
