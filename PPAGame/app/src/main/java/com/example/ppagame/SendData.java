package com.example.ppagame;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by bala on 30.03.2018.
 */

public class SendData implements Runnable
{
    public ObjectOutputStream objectOutputStream;
    public ArrayList<Object>date;
    public SendData(ObjectOutputStream objectOutputStream,ArrayList<Object> date)
    {
        this.objectOutputStream=objectOutputStream;
        this.date=date;
    }
    @Override
    public void run()
    {
        try {
            objectOutputStream.writeObject(date);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
