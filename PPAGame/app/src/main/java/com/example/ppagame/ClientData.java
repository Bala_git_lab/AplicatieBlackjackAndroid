package com.example.ppagame;
import java.io.Serializable;
public class ClientData implements Serializable
{
    private String username;
    private String password;
    public boolean is_registered;
    public boolean acces_granted;
    public ClientData(String username,String password)
    {
        this.username=username;
        this.password=password;
        is_registered=false;
        acces_granted=false;
    }
    public String getUsername()
    {
        return this.username;
    }
    public String getPassword()
    {
        return this.password;
    }
}
