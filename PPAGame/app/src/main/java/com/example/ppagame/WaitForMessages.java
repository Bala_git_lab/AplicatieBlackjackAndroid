package com.example.ppagame;

import android.content.Intent;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Created by bala on 14.04.2018.
 */

public class WaitForMessages implements Runnable
{
    public ObjectInputStream objectInputStream;
    public boolean exit;
    public ArrayList<Object>date;
    private Thread game_thread;
    private Game game;
    public Thread animatie_primire_mesaj;
    public WaitForMessages(ObjectInputStream objectInputStream,Thread game_thread,Game game)
    {
        this.objectInputStream=objectInputStream;
        this.game_thread=game_thread;
        this.game=game;
        exit=false;
        date=new ArrayList<>();
    }
    @Override
    public void run()
    {
        while (true) {
            date = new ArrayList<>();
            String comanda;
            String mesaj;
            try {
                date = (ArrayList<Object>) objectInputStream.readObject();
                comanda = (String) date.get(0);
                if (comanda.equals("mesaj"))
                {
                        mesaj = (String) date.get(1);
                        Game_Activity.mesaj_primit = mesaj;
                        Message message = Message.obtain();
                        message.arg1 = 14;
                        Game_Activity.handler.sendMessage(message);

                        //vreau sa maresc bubble-ul cu mesajul si sa il micsorez putin(sa am un efect)
                        animatie_primire_mesaj = new Thread(new EfectSpecialMesaj());
                        animatie_primire_mesaj.start();

                }
                else
                {
                    if(comanda.equals("finish thread"))
                    {
                        Log.d("WaitForMessages:", "Nu mai astept mesaje");
                        break;
                    }
                    else
                    {
                        if(comanda.equals("exit"))
                        {
                            game.command="exit";
                            game_thread.interrupt();
                            Intent intent = new Intent(game.context, GameMenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            game.context.startActivity(intent);//pornesc meniul
                            break;
                        }
                        else//e o carte
                        {
                            game.date = date;
                            game_thread.interrupt();
                        }
                    }
                }
            }
            catch (IOException e) {
                e.printStackTrace();
                break;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
