package com.example.ppagame;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by bala on 28.04.2018.
 */

public class SendMessages implements Runnable
{
    public ObjectOutputStream objectOutputStream;
    public String command;
    public String mesaj;
    public ArrayList<Object> date;
    public Thread game_thread;
    public SendMessages(ObjectOutputStream objectOutputStream,Thread game_thread)
    {
        this.objectOutputStream=objectOutputStream;
        date=new ArrayList<>();
        this.game_thread=game_thread;
    }
    @Override
    public void run() {
        while(true)
        {
            try
            {
                Thread.sleep(999999999);
            }
            catch (InterruptedException e)
            {
                Log.d("SendMessagesThread: ", "was interrupted");
                e.printStackTrace();
            }
            if(command.equals("trimite mesaj"))
            {
                date=new ArrayList<>();
                command="mesaj";
                date.add(command);
                date.add(mesaj);
                try {
                    objectOutputStream.writeObject(date);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            else
            {
                if(command.equals("finish thread"))//ii zic threadului de la server care asteapta mesajele sa se opreasca
                {
                    date=new ArrayList<>();
                    date.add(command);
                    try {
                        objectOutputStream.writeObject(date);
                        Log.d("Send Messages Thread:", "Am trimis mesaj catre server ca sa nu mai astepte mesaje");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    break;
                }
                else//exit
                {
                    game_thread.interrupt();
                    date=new ArrayList<>();
                    date.add(command);
                    try
                    {
                        objectOutputStream.writeObject(date);
                        Log.d("Send Messages Thread:", "Am trimis mesaj catre server ca vreau sa ies din joc");
                    }
                    catch (IOException e1) {
                        e1.printStackTrace();
                    }
                        break;
                }
            }
        }
        Log.d("SendMessagesThread", "finished");
    }
}
