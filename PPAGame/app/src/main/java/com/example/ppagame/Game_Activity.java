package com.example.ppagame;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Game_Activity extends AppCompatActivity {
    public Thread startGame;
    public BroadcastReceiver two_cards_broadcastReceiver;

    public TextView opponent_username_text_view;
    public TextView my_username_text_view;
    public ImageButton my_right_card;
    public ImageButton my_left_card;
    public ImageView player_card1;
    public ImageView player_card2;
    public ImageView player_card3;
    public ImageButton opponent_left_card;
    public ImageButton opponent_right_card;
    public ImageView opponent_card1;
    public ImageView opponent_card2;
    public ImageView opponent_card3;
    public Button hit_button;
    public Button stay_button;
    public TextView my_status_text;
    public TextView enemy_status_text;
    public TextView win_text;
    public ImageView my_message_bubble;
    public ImageView opponent_message_bubble;
    public Button write_message_button;
    public ImageView write_message_backround;
    public EditText message_edit_text;
    public Button send_message_button;
    public TextView my_text_in_bubble;
    public TextView opponent_text_in_bubble;
    public Button exit_button;
    public String message;


    public ObjectOutputStream objectOutputStream;
    public ObjectInputStream objectInputStream;
    public Context context;
    public static UserData my_user_data;
    public static UserData opponent_user_data;
    public boolean game_finished;
    public boolean write_message;

    //players data begins
    public static volatile String right_card;
    public static volatile String left_card;
    public static volatile String enemy_right_card;
    public static volatile String enemy_left_card;
    public static volatile String my_card;
    public static volatile String opponent_card;
    public static volatile int my_coins;
    public static volatile int enemy_coins;
    public static volatile int my_cards_sum1;
    public static volatile int my_cards_sum2;
    public static volatile int enemy_cards_sum1;
    public static volatile int enemy_cards_sum2;
    public static volatile String mesaj_primit;
    //players data ends

    public static Handler handler;
    public static Handler handler2;
    public static Handler handler3;

    public Thread message_animation;

    public boolean received_cards;
    public static volatile boolean I_start_first;
    public static volatile boolean my_turn;
    public float increment=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_game_);
        my_user_data=GameMenuActivity.userData;
        opponent_user_data=GameMenuActivity.opponent_data;
        objectOutputStream=MainActivity.objectOutputStream;
        objectInputStream=MainActivity.objectInputStream;
        received_cards=false;
        I_start_first=false;
        my_turn=false;
        write_message=false;
        my_coins=500;
        enemy_coins=500;
        context=(Context)this;

        initUI();
        game_finished=false;


        final Game game=new Game(objectOutputStream,objectInputStream,getApplicationContext());
        startGame=new Thread(game);
        startGame.start();//pornesc o noua mana de joc



        handler=new Handler()//am creat un handler si astfel pot sa trimit mesaje din threadul jocului
        {
            @Override
            public void handleMessage(Message mesaj)
            {
                if (mesaj.arg1==0)//primesc cartile din stanga
                {
                    setCard(my_left_card,left_card);
                    setCard(opponent_left_card,enemy_left_card);
                    my_left_card.setVisibility(View.VISIBLE);
                    opponent_left_card.setVisibility(View.VISIBLE);
                }
                if(mesaj.arg1==1)//primesc cartile din dreapta
                {
                    if(I_start_first==true)
                    {
                        setCard(my_right_card,right_card);
                        stay_button.setVisibility(View.VISIBLE);
                        hit_button.setVisibility(View.VISIBLE);
                        my_status_text.setVisibility(TextView.VISIBLE);
                        enemy_status_text.setVisibility(TextView.INVISIBLE);
                    }
                    else
                    {
                        setCard(opponent_right_card,enemy_right_card);
                        stay_button.setVisibility(Button.INVISIBLE);
                        hit_button.setVisibility(Button.INVISIBLE);
                        my_status_text.setVisibility(TextView.INVISIBLE);
                        enemy_status_text.setVisibility(TextView.VISIBLE);
                    }
                    my_right_card.setVisibility(ImageButton.VISIBLE);
                    opponent_right_card.setVisibility(ImageButton.VISIBLE);
                }
                if(mesaj.arg1==2)//adaug cartea primita de la server si dau update la suma
                {
                    if(game.nr_carti==1)
                    {
                        setCard(player_card1, my_card);
                        player_card1.setVisibility(View.VISIBLE);
                    }
                    if(game.nr_carti==2)
                    {
                        setCard(player_card2, my_card);
                        player_card2.setVisibility(View.VISIBLE);
                    }
                    if(game.nr_carti==3)
                    {
                        setCard(player_card3, my_card);
                        player_card3.setVisibility(View.VISIBLE);
                    }
                    if((my_cards_sum2>21)||(my_cards_sum2==my_cards_sum1))
                        my_status_text.setText(""+my_cards_sum1);
                    else
                        my_status_text.setText(""+my_cards_sum1+"/"+my_cards_sum2);
                    Log.d("GameActivity", "handleMessage:Carte:"+my_card);
                }
                if(mesaj.arg1==3)//adaug cartea primita de la server a adversarului si dau update la suma cartilor adversarului
                {
                    if(game.nr_carti==1)
                    {
                        setCard(opponent_card1,opponent_card);
                        opponent_card1.setVisibility(View.VISIBLE);
                    }
                    if(game.nr_carti==2)
                    {
                        setCard(opponent_card2,opponent_card);
                        opponent_card2.setVisibility(View.VISIBLE);
                    }
                    if(game.nr_carti==3)
                    {
                        setCard(opponent_card3,opponent_card);
                        opponent_card3.setVisibility(View.VISIBLE);
                    }
                    if((enemy_cards_sum2>21)||(enemy_cards_sum2==enemy_cards_sum1))
                        enemy_status_text.setText(""+enemy_cards_sum1);
                    else
                        enemy_status_text.setText(""+enemy_cards_sum1+"/"+enemy_cards_sum2);
                    Log.d("GameActivity", "handleMessage:Carte:"+opponent_card);
                }
                if(mesaj.arg1==4)//afisez suma cartilor initiala arat butoanele si arat cartea dreapta(atunci cand nu incep primul dar e randul meu)
                {
                    hit_button.setVisibility(Button.VISIBLE);
                    stay_button.setVisibility(Button.VISIBLE);
                    my_status_text.setVisibility(TextView.VISIBLE);
                    setCard(my_right_card,right_card);
                }
                if(mesaj.arg1==5)//afisez suma cartilor initiala
                {
                        String text;
                        if(Game_Activity.my_cards_sum2==21)
                        {
                            text = "" + Game_Activity.my_cards_sum2;
                        }
                        else
                        {
                            if(Game_Activity.my_cards_sum2==Game_Activity.my_cards_sum1)
                                text = "" + Game_Activity.my_cards_sum1;
                            else
                                text = "" + Game_Activity.my_cards_sum1+"/"+Game_Activity.my_cards_sum2;
                        }
                        my_status_text.setText(text);
                        if(Game_Activity.enemy_cards_sum2==21) {
                            text = "" + Game_Activity.enemy_cards_sum2;
                        }
                        else
                        {
                            if(Game_Activity.enemy_cards_sum1==Game_Activity.enemy_cards_sum2)
                                text = "" + Game_Activity.enemy_cards_sum1;
                            else
                                text = "" + Game_Activity.enemy_cards_sum1+"/"+Game_Activity.enemy_cards_sum2;
                        }
                        enemy_status_text.setText(text);
                }
                if(mesaj.arg1==6)//am facut BlackJack
                {
                    my_status_text.setText("BlackJack");
                    hit_button.setVisibility(Button.INVISIBLE);
                    stay_button.setVisibility(Button.INVISIBLE);
                }
                if(mesaj.arg1==7)//am avut prea mare suma
                {
                    my_status_text.setText("Busted");
                    hit_button.setVisibility(Button.INVISIBLE);
                    stay_button.setVisibility(Button.INVISIBLE);
                }
                if(mesaj.arg1==8)//am dat stay
                {
                    hit_button.setVisibility(Button.INVISIBLE);
                    stay_button.setVisibility(Button.INVISIBLE);
                }
                if(mesaj.arg1==9)//intorc cartea adversarului ca sa vad ambele carti si afisez prima oara suma
                {
                    setCard(opponent_right_card,enemy_right_card);
                    opponent_left_card.setVisibility(View.VISIBLE);
                    enemy_status_text.setVisibility(TextView.VISIBLE);
                    //enemy_status_text.setText(""+enemy_cards_sum1+"/"+enemy_cards_sum2);//am afisat suma cartilor playerului 2
                }
                if(mesaj.arg1==10)//adversarul a facut BlackJack
                {
                    enemy_status_text.setText("BlackJack");
                }
                if(mesaj.arg1==11)//adversarul a avut suma cartilor prea mare
                {
                    enemy_status_text.setText("Busted");
                }
                if(mesaj.arg1==12)//reinitializez datele(adversarul va incepe runda)
                {
                    player_card1.setVisibility(View.INVISIBLE);
                    player_card2.setVisibility(View.INVISIBLE);
                    player_card3.setVisibility(View.INVISIBLE);
                    opponent_card1.setVisibility(View.INVISIBLE);
                    opponent_card2.setVisibility(View.INVISIBLE);
                    opponent_card3.setVisibility(View.INVISIBLE);
                    my_status_text.setText("");
                    my_status_text.setVisibility(TextView.INVISIBLE);
                    enemy_status_text.setText("");
                    enemy_status_text.setVisibility(TextView.INVISIBLE);
                    opponent_left_card.setVisibility(ImageButton.INVISIBLE);
                    opponent_right_card.setVisibility(ImageButton.INVISIBLE);
                    my_right_card.setVisibility(ImageButton.INVISIBLE);
                    my_left_card.setVisibility(ImageButton.INVISIBLE);
                    win_text.setVisibility(TextView.INVISIBLE);
                    flipCardOnBack(my_right_card);
                }
                if(mesaj.arg1==13)//reinitializez datele(eu voi incepe runda)
                {
                    player_card1.setVisibility(View.INVISIBLE);
                    player_card2.setVisibility(View.INVISIBLE);
                    player_card3.setVisibility(View.INVISIBLE);
                    opponent_card1.setVisibility(View.INVISIBLE);
                    opponent_card2.setVisibility(View.INVISIBLE);
                    opponent_card3.setVisibility(View.INVISIBLE);
                    my_status_text.setVisibility(TextView.INVISIBLE);
                    enemy_status_text.setVisibility(TextView.INVISIBLE);
                    win_text.setVisibility(TextView.INVISIBLE);
                    my_right_card.setVisibility(ImageButton.INVISIBLE);
                    my_left_card.setVisibility(ImageButton.INVISIBLE);
                    opponent_left_card.setVisibility(ImageButton.INVISIBLE);
                    opponent_right_card.setVisibility(ImageButton.INVISIBLE);
                    flipCardOnBack(opponent_right_card);
                }
                if(mesaj.arg1==14)
                {
                        float initialXSize=0.048f;
                        float initialYSize=-0.048f;
                        opponent_message_bubble.setVisibility(ImageView.VISIBLE);
                        opponent_text_in_bubble.setText(mesaj_primit);
                        opponent_text_in_bubble.setVisibility(TextView.VISIBLE);
                        opponent_message_bubble.setScaleX(initialXSize);
                        opponent_message_bubble.setScaleY(initialYSize);
                        opponent_text_in_bubble.setScaleX(0.048f);
                        opponent_text_in_bubble.setScaleY(0.048f);
                }
                if(mesaj.arg1==15)
                {
                    float initialXSize=0.048f;
                    float initialYSize=-0.048f;
                    if(mesaj.arg2==0)//mareste mesajul
                    {
                        opponent_message_bubble.setScaleX(opponent_message_bubble.getScaleX()+initialXSize);
                        opponent_message_bubble.setScaleY(opponent_message_bubble.getScaleY()+initialYSize);
                        opponent_text_in_bubble.setScaleX(opponent_text_in_bubble.getScaleX()+initialXSize);
                        opponent_text_in_bubble.setScaleY(opponent_text_in_bubble.getScaleY()-initialYSize);
                    }
                    else//micsoreaza mesajul
                    {
                        opponent_message_bubble.setScaleX(opponent_message_bubble.getScaleX()-0.04f);
                        opponent_message_bubble.setScaleY(opponent_message_bubble.getScaleY()+0.04f);
                        opponent_text_in_bubble.setScaleX(opponent_text_in_bubble.getScaleX()-0.04f);
                        opponent_text_in_bubble.setScaleY(opponent_text_in_bubble.getScaleY()-0.04f);
                    }
                }
                if(mesaj.arg1==16)
                {
                    opponent_message_bubble.setVisibility(ImageView.INVISIBLE);
                    opponent_text_in_bubble.setVisibility(TextView.INVISIBLE);
                }
                super.handleMessage(mesaj);
            }
        };

        handler2=new Handler()
        {
            @Override
            public void handleMessage(Message mesaj)
            {
                if(mesaj.arg1==0)//s-a stabilit castigatorul si am castigat-ascund butoanele si setez textul de feedback
                {
                    //ascund butoanele
                    hit_button.setVisibility(Button.INVISIBLE);
                    stay_button.setVisibility(Button.INVISIBLE);
                    //setez textul de feedback si alte lucruri
                    my_message_bubble.setVisibility(ImageView.INVISIBLE);
                    opponent_message_bubble.setVisibility(ImageView.INVISIBLE);
                    my_text_in_bubble.setVisibility(TextView.INVISIBLE);
                    opponent_text_in_bubble.setVisibility(TextView.INVISIBLE);
                    win_text.setTextColor(Color.GREEN);
                    win_text.setVisibility(TextView.VISIBLE);
                    win_text.setText("You won");
                }
                if(mesaj.arg1==1)//s-a stabilit castigatorul si am castigat-ascund butoanele si setez textul de feedback
                {
                    //ascund butoanele
                    hit_button.setVisibility(Button.INVISIBLE);
                    stay_button.setVisibility(Button.INVISIBLE);
                    //setez textul de feedback si alte lucruri
                    my_message_bubble.setVisibility(ImageView.INVISIBLE);
                    opponent_message_bubble.setVisibility(ImageView.INVISIBLE);
                    my_text_in_bubble.setVisibility(TextView.INVISIBLE);
                    opponent_text_in_bubble.setVisibility(TextView.INVISIBLE);
                    win_text.setTextColor(Color.BLUE);
                    win_text.setVisibility(TextView.VISIBLE);
                    win_text.setText("Draw");
                }
                if(mesaj.arg1==2)//s-a stabilit castigatorul si am castigat-ascund butoanele si setez textul de feedback
                {
                    //ascund butoanele
                    hit_button.setVisibility(Button.INVISIBLE);
                    stay_button.setVisibility(Button.INVISIBLE);
                    //setez textul de feedback si alte lucruri
                    my_message_bubble.setVisibility(ImageView.INVISIBLE);
                    opponent_message_bubble.setVisibility(ImageView.INVISIBLE);
                    my_text_in_bubble.setVisibility(TextView.INVISIBLE);
                    opponent_text_in_bubble.setVisibility(TextView.INVISIBLE);
                    win_text.setTextColor(Color.RED);
                    win_text.setVisibility(TextView.VISIBLE);
                    win_text.setText("You lost");
                }
                if(mesaj.arg1==3)//efectul de marire al textului cand castig
                {
                    if (mesaj.arg2==0)//marire text
                    {
                        win_text.setTextSize(10+increment);
                        increment++;
                        if(increment==16)
                            increment=1;
                    }
                    if (mesaj.arg2==1)//micsorare text
                    {
                        win_text.setTextSize(25-increment);
                        increment++;
                        if(increment==16)
                            increment=1;
                    }
                }
                super.handleMessage(mesaj);
            }
        };
        handler3=new Handler(){
          @Override
          public void handleMessage(Message mesaj)
          {
              if(mesaj.arg1==0)
              {
                  float initialXSize = 0.048f;
                  float initialYSize = 0.048f;
                  if (mesaj.arg2 == 0)//mareste mesajul
                  {
                      my_message_bubble.setScaleX(my_message_bubble.getScaleX() + initialXSize);
                      my_message_bubble.setScaleY(my_message_bubble.getScaleY() + initialYSize);
                      my_text_in_bubble.setScaleX(my_text_in_bubble.getScaleX() + initialXSize);
                      my_text_in_bubble.setScaleY(my_text_in_bubble.getScaleY() + initialYSize);
                  }
                  else//micsoreaza mesajul
                  {
                      my_message_bubble.setScaleX(my_message_bubble.getScaleX() - 0.04f);
                      my_message_bubble.setScaleY(my_message_bubble.getScaleY() - 0.04f);
                      my_text_in_bubble.setScaleX(my_text_in_bubble.getScaleX() - 0.04f);
                      my_text_in_bubble.setScaleY(my_text_in_bubble.getScaleY() - 0.04f);
                  }
              }
              if(mesaj.arg1==1)
              {
                  my_message_bubble.setVisibility(ImageView.INVISIBLE);
                  my_text_in_bubble.setVisibility(TextView.INVISIBLE);
              }
              super.handleMessage(mesaj);
            }
        };


        hit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Log.d("Game Activity:", "onClick: ");
                    if (my_turn == true)
                    {
                        game.command="hit";
                        startGame.interrupt();
                    }
            }
        });
        stay_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Game Activity:", "onClick: ");
                if (my_turn == true)
                {
                    game.command="stay";

                    startGame.interrupt();
                }
            }
        });

        send_message_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(my_turn==true) {
                    write_message = false;
                    write_message_backround.setVisibility(ImageView.INVISIBLE);
                    message_edit_text.setVisibility(EditText.INVISIBLE);
                    send_message_button.setVisibility(Button.INVISIBLE);
                    write_message_button.setText("Write Message");
                    message = message_edit_text.getText().toString();
                    message_edit_text.setText("");
                    my_message_bubble.setVisibility(ImageView.VISIBLE);
                    my_message_bubble.setScaleX(0.048f);
                    my_message_bubble.setScaleY(0.048f);
                    my_text_in_bubble.setText(message);
                    my_text_in_bubble.setVisibility(TextView.VISIBLE);
                    my_text_in_bubble.setScaleX(0.048f);
                    my_text_in_bubble.setScaleY(0.048f);
                    message_animation=new Thread(new EfectSpecialMesaj2());
                    message_animation.start();
                    game.command = "trimite mesaj";
                    game.message = message;
                    startGame.interrupt();
                    //hideSoftKeyboard(Game_Activity.this);
                }
                else
                {
                    write_message = false;
                    write_message_backround.setVisibility(ImageView.INVISIBLE);
                    message_edit_text.setVisibility(EditText.INVISIBLE);
                    send_message_button.setVisibility(Button.INVISIBLE);
                    write_message_button.setText("Write Message");
                    message = message_edit_text.getText().toString();
                    message_edit_text.setText("");
                    my_message_bubble.setVisibility(ImageView.VISIBLE);
                    my_message_bubble.setScaleX(0.048f);
                    my_message_bubble.setScaleY(0.048f);
                    my_text_in_bubble.setText(message);
                    my_text_in_bubble.setVisibility(TextView.VISIBLE);
                    my_text_in_bubble.setScaleX(0.048f);
                    my_text_in_bubble.setScaleY(0.048f);
                    message_animation=new Thread(new EfectSpecialMesaj2());
                    message_animation.start();
                    game.sendMessagesRunnable.command="trimite mesaj";
                    game.sendMessagesRunnable.mesaj=message;
                    game.sendMessagesThread.interrupt();
                    //hideSoftKeyboard(Game_Activity.this);
                }
            }
        });

        write_message_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(write_message==false)
                {
                    write_message=true;
                    write_message_backround.setVisibility(ImageView.VISIBLE);
                    message_edit_text.setVisibility(EditText.VISIBLE);
                    send_message_button.setVisibility(Button.VISIBLE);
                    write_message_button.setText("Exit");
                }
                else
                {
                    write_message=false;
                    write_message_backround.setVisibility(ImageView.INVISIBLE);
                    message_edit_text.setVisibility(EditText.INVISIBLE);
                    send_message_button.setVisibility(Button.INVISIBLE);
                    write_message_button.setText("Write Message");
                }
            }
        });

        exit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(my_turn==true) {
                    game.command = "exit";
                    startGame.interrupt();
                    Intent intent = new Intent(context, GameMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                else
                {
                    game.sendMessagesRunnable.command="exit";
                    game.sendMessagesThread.interrupt();
                    Intent intent = new Intent(context, GameMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });

    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }


    public void initUI()
    {
        my_username_text_view=(TextView)findViewById(R.id.my_username);
        my_username_text_view.setText(my_user_data.username);

        opponent_username_text_view=(TextView)findViewById(R.id.opponent_username);
        opponent_username_text_view.setText(opponent_user_data.username);

        my_right_card=(ImageButton)findViewById(R.id.my_right_card);
        my_right_card.setVisibility(View.INVISIBLE);
        my_right_card.getBackground().setAlpha(0);

        my_left_card=(ImageButton)findViewById(R.id.my_left_card);
        my_left_card.setVisibility(View.INVISIBLE);
        my_left_card.getBackground().setAlpha(0);

        player_card1=(ImageView)findViewById(R.id.player_card1);
        player_card1.setVisibility(View.INVISIBLE);

        player_card2=(ImageView)findViewById(R.id.player_card2);
        player_card2.setVisibility(View.INVISIBLE);

        player_card3=(ImageView)findViewById(R.id.player_card3);
        player_card3.setVisibility(View.INVISIBLE);

        opponent_left_card=(ImageButton)findViewById(R.id.opponent_left_card);
        opponent_left_card.setVisibility(View.INVISIBLE);
        opponent_left_card.getBackground().setAlpha(0);

        opponent_right_card=(ImageButton)findViewById(R.id.opponent_right_card);
        opponent_right_card.setVisibility(View.INVISIBLE);
        opponent_right_card.getBackground().setAlpha(0);

        opponent_card1=(ImageView)findViewById(R.id.opponent_card1);
        opponent_card1.setVisibility(View.INVISIBLE);

        opponent_card2=(ImageView)findViewById(R.id.opponent_card2);
        opponent_card2.setVisibility(View.INVISIBLE);

        opponent_card3=(ImageView)findViewById(R.id.opponent_card3);
        opponent_card3.setVisibility(View.INVISIBLE);

        hit_button=(Button)findViewById(R.id.hit_button);
        hit_button.setBackgroundColor(android.graphics.Color.argb(200,255,213,0));
        stay_button=(Button)findViewById(R.id.stay_button);
        stay_button.setBackgroundColor(android.graphics.Color.argb(200,255,213,0));

        my_status_text=(TextView)findViewById(R.id.my_status_text);
        enemy_status_text=(TextView)findViewById(R.id.opponent_status_text);
        win_text=(TextView)findViewById(R.id.win_text);
        win_text.setTextSize(16);


        my_message_bubble=(ImageView)findViewById(R.id.my_message_bubble);
        my_message_bubble.setVisibility(ImageView.INVISIBLE);

        opponent_message_bubble=(ImageView)findViewById(R.id.opponent_message_bubble);
        opponent_message_bubble.setVisibility(ImageView.INVISIBLE);

        write_message_button=(Button)findViewById(R.id.write_message_button);
        write_message_button.setBackgroundColor(android.graphics.Color.argb(200,255,213,0));

        write_message_backround=(ImageView)findViewById(R.id.message_backround);
        write_message_backround.setBackgroundColor(android.graphics.Color.argb(200,255,213,0));
        write_message_backround.setVisibility(ImageView.INVISIBLE);

        message_edit_text=(EditText)findViewById(R.id.write_message_edit_text);
        message_edit_text.setVisibility(EditText.INVISIBLE);

        send_message_button=(Button)findViewById(R.id.send_message_button);
        send_message_button.setVisibility(Button.INVISIBLE);

        my_text_in_bubble=(TextView)findViewById(R.id.my_text_in_bubble);

        opponent_text_in_bubble=(TextView)findViewById(R.id.opponent_text_in_bubble);

        exit_button=(Button)findViewById(R.id.exit_button);
        exit_button.setBackgroundColor(android.graphics.Color.argb(200,228,6,43));
    }


    public void setCard(ImageButton card,String carte_nume)
    {
        switch(carte_nume)
        {
            case "2_inima-neagra":
                card.setImageResource(R.drawable.tile000);
                break;
            case "3_inima-neagra":
                card.setImageResource(R.drawable.tile001);
                break;
            case "4_inima-neagra":
                card.setImageResource(R.drawable.tile002);
                break;
            case "5_inima-neagra":
                card.setImageResource(R.drawable.tile003);
                break;
            case "6_inima-neagra":
                card.setImageResource(R.drawable.tile004);
                break;
            case "7_inima-neagra":
                card.setImageResource(R.drawable.tile005);
                break;
            case "8_inima-neagra":
                card.setImageResource(R.drawable.tile006);
                break;
            case "9_inima-neagra":
                card.setImageResource(R.drawable.tile007);
                break;
            case "10_inima-neagra":
                card.setImageResource(R.drawable.tile008);
                break;
            case "valet_inima-neagra":
                card.setImageResource(R.drawable.tile009);
                break;
            case "dama_inima-neagra":
                card.setImageResource(R.drawable.tile010);
                break;
            case "popa_inima-neagra":
                card.setImageResource(R.drawable.tile011);
                break;
            case "as_inima-neagra":
                card.setImageResource(R.drawable.tile012);
                break;

            case "2_inima-rosie":
                card.setImageResource(R.drawable.tile014);
                break;
            case "3_inima-rosie":
                card.setImageResource(R.drawable.tile015);
                break;
            case "4_inima-rosie":
                card.setImageResource(R.drawable.tile016);
                break;
            case "5_inima-rosie":
                card.setImageResource(R.drawable.tile017);
                break;
            case "6_inima-rosie":
                card.setImageResource(R.drawable.tile018);
                break;
            case "7_inima-rosie":
                card.setImageResource(R.drawable.tile019);
                break;
            case "8_inima-rosie":
                card.setImageResource(R.drawable.tile020);
                break;
            case "9_inima-rosie":
                card.setImageResource(R.drawable.tile021);
                break;
            case "10_inima-rosie":
                card.setImageResource(R.drawable.tile022);
                break;
            case "valet_inima-rosie":
                card.setImageResource(R.drawable.tile023);
                break;
            case "dama_inima-rosie":
                card.setImageResource(R.drawable.tile024);
                break;
            case "popa_inima-rosie":
                card.setImageResource(R.drawable.tile025);
                break;
            case "as_inima-rosie":
                card.setImageResource(R.drawable.tile026);
                break;

            case "2_romb":
                card.setImageResource(R.drawable.tile028);
                break;
            case "3_romb":
                card.setImageResource(R.drawable.tile029);
                break;
            case "4_romb":
                card.setImageResource(R.drawable.tile030);
                break;
            case "5_romb":
                card.setImageResource(R.drawable.tile031);
                break;
            case "6_romb":
                card.setImageResource(R.drawable.tile032);
                break;
            case "7_romb":
                card.setImageResource(R.drawable.tile033);
                break;
            case "8_romb":
                card.setImageResource(R.drawable.tile034);
                break;
            case "9_romb":
                card.setImageResource(R.drawable.tile035);
                break;
            case "10_romb":
                card.setImageResource(R.drawable.tile036);
                break;
            case "valet_romb":
                card.setImageResource(R.drawable.tile037);
                break;
            case "dama_romb":
                card.setImageResource(R.drawable.tile038);
                break;
            case "popa_romb":
                card.setImageResource(R.drawable.tile039);
                break;
            case "as_romb":
                card.setImageResource(R.drawable.tile040);
                break;

            case "2_trefla":
                card.setImageResource(R.drawable.tile042);
                break;
            case "3_trefla":
                card.setImageResource(R.drawable.tile043);
                break;
            case "4_trefla":
                card.setImageResource(R.drawable.tile044);
                break;
            case "5_trefla":
                card.setImageResource(R.drawable.tile045);
                break;
            case "6_trefla":
                card.setImageResource(R.drawable.tile046);
                break;
            case "7_trefla":
                card.setImageResource(R.drawable.tile047);
                break;
            case "8_trefla":
                card.setImageResource(R.drawable.tile048);
                break;
            case "9_trefla":
                card.setImageResource(R.drawable.tile049);
                break;
            case "10_trefla":
                card.setImageResource(R.drawable.tile050);
                break;
            case "valet_trefla":
                card.setImageResource(R.drawable.tile051);
                break;
            case "dama_trefla":
                card.setImageResource(R.drawable.tile052);
                break;
            case "popa_trefla":
                card.setImageResource(R.drawable.tile053);
                break;
            case "as_trefla":
                card.setImageResource(R.drawable.tile054);
                break;
        }

    }
    public void setCard(ImageView card,String carte_nume)
    {
        switch(carte_nume)
        {
            case "2_inima-neagra":
                card.setImageResource(R.drawable.tile000);
                break;
            case "3_inima-neagra":
                card.setImageResource(R.drawable.tile001);
                break;
            case "4_inima-neagra":
                card.setImageResource(R.drawable.tile002);
                break;
            case "5_inima-neagra":
                card.setImageResource(R.drawable.tile003);
                break;
            case "6_inima-neagra":
                card.setImageResource(R.drawable.tile004);
                break;
            case "7_inima-neagra":
                card.setImageResource(R.drawable.tile005);
                break;
            case "8_inima-neagra":
                card.setImageResource(R.drawable.tile006);
                break;
            case "9_inima-neagra":
                card.setImageResource(R.drawable.tile007);
                break;
            case "10_inima-neagra":
                card.setImageResource(R.drawable.tile008);
                break;
            case "valet_inima-neagra":
                card.setImageResource(R.drawable.tile009);
                break;
            case "dama_inima-neagra":
                card.setImageResource(R.drawable.tile010);
                break;
            case "popa_inima-neagra":
                card.setImageResource(R.drawable.tile011);
                break;
            case "as_inima-neagra":
                card.setImageResource(R.drawable.tile012);
                break;

            case "2_inima-rosie":
                card.setImageResource(R.drawable.tile014);
                break;
            case "3_inima-rosie":
                card.setImageResource(R.drawable.tile015);
                break;
            case "4_inima-rosie":
                card.setImageResource(R.drawable.tile016);
                break;
            case "5_inima-rosie":
                card.setImageResource(R.drawable.tile017);
                break;
            case "6_inima-rosie":
                card.setImageResource(R.drawable.tile018);
                break;
            case "7_inima-rosie":
                card.setImageResource(R.drawable.tile019);
                break;
            case "8_inima-rosie":
                card.setImageResource(R.drawable.tile020);
                break;
            case "9_inima-rosie":
                card.setImageResource(R.drawable.tile021);
                break;
            case "10_inima-rosie":
                card.setImageResource(R.drawable.tile022);
                break;
            case "valet_inima-rosie":
                card.setImageResource(R.drawable.tile023);
                break;
            case "dama_inima-rosie":
                card.setImageResource(R.drawable.tile024);
                break;
            case "popa_inima-rosie":
                card.setImageResource(R.drawable.tile025);
                break;
            case "as_inima-rosie":
                card.setImageResource(R.drawable.tile026);
                break;

            case "2_romb":
                card.setImageResource(R.drawable.tile028);
                break;
            case "3_romb":
                card.setImageResource(R.drawable.tile029);
                break;
            case "4_romb":
                card.setImageResource(R.drawable.tile030);
                break;
            case "5_romb":
                card.setImageResource(R.drawable.tile031);
                break;
            case "6_romb":
                card.setImageResource(R.drawable.tile032);
                break;
            case "7_romb":
                card.setImageResource(R.drawable.tile033);
                break;
            case "8_romb":
                card.setImageResource(R.drawable.tile034);
                break;
            case "9_romb":
                card.setImageResource(R.drawable.tile035);
                break;
            case "10_romb":
                card.setImageResource(R.drawable.tile036);
                break;
            case "valet_romb":
                card.setImageResource(R.drawable.tile037);
                break;
            case "dama_romb":
                card.setImageResource(R.drawable.tile038);
                break;
            case "popa_romb":
                card.setImageResource(R.drawable.tile039);
                break;
            case "as_romb":
                card.setImageResource(R.drawable.tile040);
                break;

            case "2_trefla":
                card.setImageResource(R.drawable.tile042);
                break;
            case "3_trefla":
                card.setImageResource(R.drawable.tile043);
                break;
            case "4_trefla":
                card.setImageResource(R.drawable.tile044);
                break;
            case "5_trefla":
                card.setImageResource(R.drawable.tile045);
                break;
            case "6_trefla":
                card.setImageResource(R.drawable.tile046);
                break;
            case "7_trefla":
                card.setImageResource(R.drawable.tile047);
                break;
            case "8_trefla":
                card.setImageResource(R.drawable.tile048);
                break;
            case "9_trefla":
                card.setImageResource(R.drawable.tile049);
                break;
            case "10_trefla":
                card.setImageResource(R.drawable.tile050);
                break;
            case "valet_trefla":
                card.setImageResource(R.drawable.tile051);
                break;
            case "dama_trefla":
                card.setImageResource(R.drawable.tile052);
                break;
            case "popa_trefla":
                card.setImageResource(R.drawable.tile053);
                break;
            case "as_trefla":
                card.setImageResource(R.drawable.tile054);
                break;
        }

    }

    public void flipCardsOnBack()
    {
        my_left_card.setImageResource(R.drawable.card_back);
        my_right_card.setImageResource(R.drawable.card_back);
    }
    public void flipCardOnBack(ImageButton card)
    {
        card.setImageResource(R.drawable.card_back);
    }
    public void hideSoftKeyboard(Activity activity)
    {
        InputMethodManager inputMethodManager = (InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed(); nu vreau sa functioneze butonul de back
    }
}