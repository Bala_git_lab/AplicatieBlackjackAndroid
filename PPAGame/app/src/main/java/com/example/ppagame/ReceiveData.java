package com.example.ppagame;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Created by bala on 30.03.2018.
 */

public class ReceiveData implements Runnable
{
    public ObjectInputStream objectInputStream;
    public ArrayList<Object> date;
    public ReceiveData(ObjectInputStream objectInputStream)
    {
        this.objectInputStream=objectInputStream;
        date=new ArrayList<>();
    }
    @Override
    public void run() {
        try {
            date=(ArrayList<Object>)objectInputStream.readObject();
            ClientData client_data=(ClientData)date.get(0);
            Log.d("Date de la server:", "isRegistered: "+client_data.is_registered+"acces_granted= "+client_data.acces_granted);
            MainActivity.is_registered=client_data.is_registered;
            MainActivity.access_granted=client_data.acces_granted;
            MainActivity.finished_thread=true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
