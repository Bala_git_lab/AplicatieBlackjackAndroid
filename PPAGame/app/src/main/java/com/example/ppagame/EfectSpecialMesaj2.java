package com.example.ppagame;

import android.os.Message;

/**
 * Created by bala on 29.04.2018.
 */

public class EfectSpecialMesaj2 implements Runnable
{
    @Override
    public void run() {
        try
        {
            Thread.sleep(20);//ca sa fiu sigur ca se face vizibil mesajul
            //voi avea 30 frame-uri la 0.3 sec
            int sleep_time = 10;
            int total_animation_time = 300;//ms
            int nr_mili_sec = 0;
            while (nr_mili_sec <= total_animation_time) {
                if (nr_mili_sec < 250) {
                    Message mesaj = Message.obtain();
                    mesaj.arg1 = 0;
                    mesaj.arg2 = 0;//marire
                    Game_Activity.handler3.sendMessage(mesaj);
                    nr_mili_sec += sleep_time;
                } else {
                    Message mesaj = Message.obtain();
                    mesaj.arg1 = 0;
                    mesaj.arg2 = 1;//micsorare
                    Game_Activity.handler3.sendMessage(mesaj);
                    nr_mili_sec += sleep_time;
                }
                Thread.sleep(sleep_time);
            }
            Thread.sleep(2000);
            Message mesaj=Message.obtain();
            mesaj.arg1=1;
            Game_Activity.handler3.sendMessage(mesaj);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
