package com.example.ppagame;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    public static Context main_activity_context;
    public  Thread connect;
    public  TextView title_text_view;
    public  TextView username_text_view;
    public  TextView password_text_view;
    public  EditText username_text_field;
    public  EditText password_text_field;
    public  RadioButton sign_in_radio_button;
    public  RadioButton sign_up_radio_button;
    public Button buton;
    public TextView feedback_text_view;
    public static boolean sign_in_was_clicked=true;
    public static boolean sign_up_was_clicked=false;
    public static volatile ObjectOutputStream objectOutputStream;
    public static volatile ObjectInputStream objectInputStream;
    public static volatile ArrayList<Object> date;
    public static volatile boolean is_registered=false;
    public static volatile boolean access_granted=true;
    public static volatile boolean finished_thread=false;
    public Thread sendData;
    public Thread receiveData;
    public static volatile UserData userData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        initUI();
        main_activity_context=(Context)this;

        connect=new Thread(new ConnectToServer());
        connect.start();



        //detectez apasarea pe sign in
        sign_in_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (objectOutputStream!=null)
                    Log.i("Game", "object_output stream is not null");
                if (sign_in_was_clicked==false) {
                        sign_in_was_clicked = true;
                        sign_up_was_clicked=false;
                        sign_up_radio_button.setChecked(false);
                        buton.setText("Sign in");

                        LoginInfo login_info = new LoginInfo(true, false);
                        date=new ArrayList<>();
                        date.add(login_info);
                        sendData=new Thread(new SendData(objectOutputStream,date));
                        sendData.start();
                    }
            }
        });

        //detectez apasarea pe sign up
        sign_up_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (sign_up_was_clicked==false)
                {
                    sign_up_was_clicked=true;
                    sign_in_was_clicked=false;
                    sign_in_radio_button.setChecked(false);
                    buton.setText("Sign up");

                    LoginInfo login_info=new LoginInfo(false,true);
                    date=new ArrayList<>();
                    date.add(login_info);
                    sendData=new Thread(new SendData(objectOutputStream,date));
                    sendData.start();
                }
            }
        });



        buton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username;
                String password;
                username=username_text_field.getText().toString();
                password=password_text_field.getText().toString();
                ClientData client_data=new ClientData(username,password);
                date=new ArrayList<>();
                date.add(client_data);


                    sendData=new Thread(new SendData(objectOutputStream,date));
                    sendData.start();//am trimis datele introduse de utilizator
                    is_registered=false;
                    access_granted=false;
                    finished_thread=false;

                    receiveData=new Thread(new ReceiveData(objectInputStream));
                    receiveData.start();

                    while(finished_thread==false)
                    {}//astept pana termina threadul de modificat valorile
                    if (sign_up_was_clicked==true)
                    {
                        //ClientData clientData=(ClientData)MainActivity.date.get(0);
                        if(is_registered==true)
                        {
                            feedback_text_view.setText("User registered!");
                            username_text_field.setText("");
                            password_text_field.setText("");
                        }
                        else
                        {
                            feedback_text_view.setText("Username is taken!");
                            username_text_field.setText("");
                            password_text_field.setText("");
                        }
                    }
                    if(sign_in_was_clicked==true)
                    {
                        ClientData clientData=(ClientData)date.get(0);
                        if (access_granted==true)
                        {
                            feedback_text_view.setText("Access granted!");
                            username_text_field.setText("");
                            password_text_field.setText("");
                            userData=new UserData(clientData.getUsername());
                            Intent intent=new Intent(MainActivity.main_activity_context,com.example.ppagame.GameMenuActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            feedback_text_view.setText("Username or password are wrong!");
                            username_text_field.setText("");
                            password_text_field.setText("");
                        }
                    }
        }
        });

    }
    public void initUI()
    {
        sign_in_radio_button=(RadioButton)findViewById(R.id.SignIn_RadioButton);
        sign_in_radio_button.setChecked(true);
        sign_in_radio_button.setTextColor(android.graphics.Color.argb(200,18,215,175));

        sign_up_radio_button=(RadioButton)findViewById(R.id.SignUp_RadioButton);
        sign_up_radio_button.setTextColor(android.graphics.Color.argb(200,18,215,175));

        title_text_view=(TextView)findViewById(R.id.Title);
        title_text_view.setTextSize(40);
        title_text_view.setTextColor(android.graphics.Color.argb(255,255,188,0));


        username_text_view=(TextView)findViewById(R.id.Username_text_view);
        username_text_view.setTextSize(17);
        username_text_view.setTextColor(android.graphics.Color.argb(255,255,188,0));

        username_text_field=(EditText)findViewById(R.id.Username_text_field);
        username_text_field.setBackgroundColor(android.graphics.Color.argb(200,18,215,175));

        password_text_view=(TextView)findViewById(R.id.Password_text_view);
        password_text_view.setTextSize(17);
        password_text_view.setTextColor(android.graphics.Color.argb(255,255,188,0));

        password_text_field=(EditText)findViewById(R.id.Pasword_text_field);
        password_text_field.setBackgroundColor(android.graphics.Color.argb(200,18,215,175));

        buton=(Button)findViewById(R.id.button);

        feedback_text_view=(TextView)findViewById(R.id.Feedback_text_view);
        feedback_text_view.setTextSize(17);
        feedback_text_view.setTextColor(Color.GREEN);
    }
}
