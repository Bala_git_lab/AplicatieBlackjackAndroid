package com.example.ppagame;

import java.io.Serializable;

public class UserData implements Serializable
{
    public String username;
    public boolean wants_to_play;
    public boolean opponent_found;
    public UserData(String username)
    {
        this.username=username;
        this.wants_to_play=false;
        this.opponent_found=false;
    }
}
