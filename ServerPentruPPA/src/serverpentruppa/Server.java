/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverpentruppa;

import java.io.IOException;
import java.net.ServerSocket;
import Threads.ConnectClients;
import com.example.ppagame.UserData;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;
/**
 *
 * @author bala
 */
public class Server
{
    public static volatile ServerSocket server_socket;
    public static volatile HashMap<String,String> conturi;
    public static volatile Stack<UserData> waiting_stack;
    public static volatile HashMap<String,ObjectOutputStream> users_output_streams;
    public static volatile HashMap<String,ObjectInputStream> users_input_streams;
    public static Thread wait_for_clients;
    public static volatile ArrayList<String> cards;
    public static void main(String[] _args) throws IOException
    {
        server_socket=new ServerSocket(7000);
        conturi=new HashMap<>();
        waiting_stack=new Stack<>();
        cards=new ArrayList<>();
        init_cards();
        users_output_streams=new HashMap<>();
        users_input_streams=new HashMap<>();
        wait_for_clients=new Thread(new ConnectClients());
        wait_for_clients.start();
    }
    public static void init_cards()
    {
        Server.cards.add("2_inima-neagra");
        Server.cards.add("3_inima-neagra");
        Server.cards.add("4_inima-neagra");
        Server.cards.add("5_inima-neagra");
        Server.cards.add("6_inima-neagra");
        Server.cards.add("7_inima-neagra");
        Server.cards.add("8_inima-neagra");
        Server.cards.add("9_inima-neagra");
        Server.cards.add("10_inima-neagra");
        Server.cards.add("valet_inima-neagra");
        Server.cards.add("dama_inima-neagra");
        Server.cards.add("popa_inima-neagra");
        Server.cards.add("as_inima-neagra");
        
        Server.cards.add("2_inima-rosie");
        Server.cards.add("3_inima-rosie");
        Server.cards.add("4_inima-rosie");
        Server.cards.add("5_inima-rosie");
        Server.cards.add("6_inima-rosie");
        Server.cards.add("7_inima-rosie");
        Server.cards.add("8_inima-rosie");
        Server.cards.add("9_inima-rosie");
        Server.cards.add("10_inima-rosie");
        Server.cards.add("valet_inima-rosie");
        Server.cards.add("dama_inima-rosie");
        Server.cards.add("popa_inima-rosie");
        Server.cards.add("as_inima-rosie");
        
        Server.cards.add("2_romb");
        Server.cards.add("3_romb");
        Server.cards.add("4_romb"); 
        Server.cards.add("5_romb");
        Server.cards.add("6_romb");
        Server.cards.add("7_romb");
        Server.cards.add("8_romb"); 
        Server.cards.add("9_romb");
        Server.cards.add("10_romb");
        Server.cards.add("valet_romb");
        Server.cards.add("dama_romb"); 
        Server.cards.add("popa_romb");
        Server.cards.add("as_romb");
        
        Server.cards.add("2_trefla");
        Server.cards.add("3_trefla");
        Server.cards.add("4_trefla");
        Server.cards.add("5_trefla");
        Server.cards.add("6_trefla");
        Server.cards.add("7_trefla");
        Server.cards.add("8_trefla");
        Server.cards.add("9_trefla");
        Server.cards.add("10_trefla");
        Server.cards.add("valet_trefla");
        Server.cards.add("dama_trefla");
        Server.cards.add("popa_trefla");
        Server.cards.add("as_trefla");
    }
}
