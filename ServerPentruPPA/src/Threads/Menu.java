/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import com.example.ppagame.UserData;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import serverpentruppa.Server;

/**
 *
 * @author bala
 */
public class Menu implements Runnable 
{
    public ObjectOutputStream object_output_stream;
    public ObjectInputStream object_input_stream;
    public ArrayList<Object> date;
    public Thread startGame;
    public Menu(ObjectOutputStream object_output_stream,ObjectInputStream object_input_stream)
    {
        this.object_output_stream=object_output_stream;
        this.object_input_stream=object_input_stream;
        date=new ArrayList<>();
    }
    @Override
    public void run() {
        try 
        {
            date=(ArrayList<Object>)this.object_input_stream.readObject();
            if(date.get(0) instanceof UserData)
            {
                UserData user_data=(UserData)date.get(0);
                if (user_data.wants_to_play)
                {
                    if (Server.waiting_stack.isEmpty())
                    {
                        user_data.wants_to_play=false;
                        Server.waiting_stack.add(user_data);
                        System.out.println("Am pus userul in asteptare");
                    }
                    else
                    {
                        user_data.wants_to_play=false;
                        user_data.opponent_found=true;
                        UserData waiting_user_data;
                        waiting_user_data = Server.waiting_stack.pop();
                        date.set(0,user_data);
                        Server.users_output_streams.get(waiting_user_data.username).writeObject(date);//am trimis datele userului curent pe output_stream-ul userului care astepta
                        waiting_user_data.opponent_found=true;
                        date.set(0,waiting_user_data);
                        object_output_stream.writeObject(date);//am trimis datele userului care astepta pe output_streamul userului curent
                        System.out.println("Am reusit sa gasesc doi playeri si sa ii conectez");
                        startGame=new Thread(new Game(object_output_stream,object_input_stream,Server.users_output_streams.get(waiting_user_data.username),Server.users_input_streams.get(waiting_user_data.username)));
                        startGame.start();//am inceput jocul si am dat ca parametrii streamurile de intrare/iesire ale ambilor jucatori
                    }
                }
            }
        } 
        catch (IOException ex) {
            System.out.println("IOException: "+ex.getMessage());
        } 
        catch (ClassNotFoundException ex) {
           System.out.println("ClassNotFoundException: "+ex.getMessage());
        }
    }
    
}
