/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import serverpentruppa.Server;
import java.io.ObjectOutputStream;
/**
 *
 * @author bala
 */
public class ConnectClients implements Runnable 
{
    public Socket socket;

    /**
     *
     */
    public ObjectOutputStream object_output_stream;
    public ObjectInputStream object_input_stream;
    public Thread login_procedure;
    @Override
    public void run()
    {
       
        while(true)
        {
            try
            {
                System.out.println("Waiting for connection...");
                socket=Server.server_socket.accept();
                System.out.println("Connected!");
                object_output_stream=new ObjectOutputStream(socket.getOutputStream());
                object_input_stream=new ObjectInputStream(socket.getInputStream());
                login_procedure=new Thread(new LoginClients(this.object_output_stream,this.object_input_stream));
                login_procedure.start();
            }
            catch (IOException ex)
            {
                Logger.getLogger(ConnectClients.class.getName()).log(Level.SEVERE, null, ex);
                break;
            }
        }     
    }
    
}
