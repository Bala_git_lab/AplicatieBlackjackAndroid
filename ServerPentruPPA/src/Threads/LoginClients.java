/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import com.example.ppagame.LoginInfo;
import com.example.ppagame.ClientData;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import serverpentruppa.Server;
/**
 *
 * @author bala
 */
public class LoginClients implements Runnable
{
    private final ObjectOutputStream object_output_stream;
    private final ObjectInputStream object_input_stream;
    private ArrayList<Object> date;
    private LoginInfo login_info;
    private Thread user_menu;
    public LoginClients(ObjectOutputStream object_output_stream,ObjectInputStream object_input_stream)
    {
        this.object_output_stream=object_output_stream;
        this.object_input_stream=object_input_stream;
    }
    @Override
    public void run()
    {
        date=new ArrayList<>();
        try
        {
            login_info=new LoginInfo(true,false);
            while(true)
            {
               if (login_info.sign_in==true)
               {
                   
                   date=(ArrayList<Object>)this.object_input_stream.readObject();
                   if(date.get(0) instanceof ClientData)
                   {
                        ClientData client_received_data=(ClientData)date.get(0);
                        System.out.printf("Date sign in: \nUsername: %s\nParola: %s\n",client_received_data.getUsername(),client_received_data.getPassword());
                        if (Server.conturi.isEmpty())
                        {
                            //nu exista utilizatori in hashmap
                            this.object_output_stream.writeObject(date);
                        }
                        else
                        {
                            if (Server.conturi.containsKey(client_received_data.getUsername()))
                            {
                                if (Server.conturi.get(client_received_data.getUsername()).equals(client_received_data.getPassword()))
                                {
                                    //parola corespunde cu cea din hashmap
                                    client_received_data.acces_granted=true;
                                    date.set(0, client_received_data);
                                    this.object_output_stream.writeObject(date);
                                    System.out.println("Access granted!");
                                    Server.users_output_streams.put(client_received_data.getUsername(), object_output_stream);
                                    Server.users_input_streams.put(client_received_data.getUsername(),object_input_stream);
                                    user_menu=new Thread(new Menu(this.object_output_stream,this.object_input_stream));
                                    user_menu.start();
                                    break;
                                }
                                else
                                {
                                    //username of password are wrong
                                    this.object_output_stream.writeObject(date);
                                    System.out.println("Username or password are wrong!");
                                }
                            }
                            else
                            {
                                //utilizatorul nu se afla in hashmap
                                this.object_output_stream.writeObject(date);
                                System.out.println("Utilizatorul nu se afla in hashmap!");
                            }
                        }
                   }
                   else
                   {
                       System.out.println("Sign up user:");
                       login_info=(LoginInfo)date.get(0);
                       continue;
                   }
               }
               if (login_info.sign_up==true)
               {
                   date=(ArrayList<Object>)this.object_input_stream.readObject();
                   if(date.get(0) instanceof ClientData)
                   {
                       ClientData client_received_data=(ClientData)date.get(0);
                       System.out.printf("Date sign up: \nUsername: %s\nParola: %s\n",client_received_data.getUsername(),client_received_data.getPassword());
                       //verific daca exista utilizatorul 
                       //daca nu exista utilizatorul in baza de date introduc utilizatorul in baza de date, isregistered se face true si trimit inapoi datele 
                       //daca exista bag utilizatorul in hashmap si trimit inapoi datele nemodificate
                       if(Server.conturi.isEmpty())
                       {
                           Server.conturi.put(client_received_data.getUsername(),client_received_data.getPassword());
                           client_received_data.is_registered=true;
                           date.set(0, client_received_data);
                           this.object_output_stream.writeObject(date);
                       }
                       else//daca se afla date in hashmap
                       {
                           if (Server.conturi.containsKey(client_received_data.getUsername()))
                           {
                               //numele de utilizator este deja luat
                               this.object_output_stream.writeObject(date);
                           }
                           else
                           {
                                Server.conturi.put(client_received_data.getUsername(),client_received_data.getPassword());
                                client_received_data.is_registered=true;
                                date.set(0, client_received_data);
                                this.object_output_stream.writeObject(date);
                           }
                       }
                   }
                   else
                   {
                       System.out.println("Sign in user:");
                       login_info=(LoginInfo)date.get(0);
                   }
               }
            }
        }
        catch (IOException ex)
        {
            System.out.println("IOException: "+ex.getMessage());
        }
        catch (ClassNotFoundException ex)
        {
           System.out.println("ClassNotFoundException: "+ex.getMessage());
        }
        }
    }

