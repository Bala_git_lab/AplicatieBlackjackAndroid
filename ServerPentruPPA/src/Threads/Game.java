/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Stack;
import serverpentruppa.Server;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author bala
 */
public class Game implements Runnable 
{
    public ObjectOutputStream player1_object_output_stream;
    public ObjectInputStream player1_object_input_stream;
    public ObjectOutputStream player2_object_output_stream;
    public ObjectInputStream player2_object_input_stream;
    public int player1_coins_total;
    public int player2_coins_total;
    public int player1_coins_in_game=500;
    public int player2_coins_in_game=500;
    public int player1_cards_sum=0;
    public int player2_cards_sum=0;
    public ArrayList<String> cards;
    public Stack<String> deck;
    public boolean game_finished;
    public static volatile ArrayList<Object> date;
    public Thread wait_for_messages;
    public Game(ObjectOutputStream player1_object_output_stream,ObjectInputStream player1_object_input_stream,ObjectOutputStream player2_object_output_stream,ObjectInputStream player2_object_input_stream)
    {
        this.player1_object_output_stream=player1_object_output_stream;
        this.player1_object_input_stream=player1_object_input_stream;
        this.player2_object_output_stream=player2_object_output_stream;
        this.player2_object_input_stream=player2_object_input_stream;
        this.game_finished=false;
        date=new ArrayList<>();
    }
    @Override
    public void run()
    {
        
        int nr;
        nr=ThreadLocalRandom.current().nextInt(0,2);//stabilesc cine incepe primul
         
        
        while(game_finished==false)
        {
            
            //aici incepe impartirea cartilor si trimiterea datelor de inceput 
            
            deck=new Stack<>();
            shuffleCards();//creeaza packetul de carti care e un stack(se amesteca cartile din arraylist si dupa sunt introduse in stiva)
            String right_card_player1;
            String left_card_player1;
            String right_card_player2;
            String left_card_player2;
            right_card_player1=deck.pop();//dau prima carte playerului 1 si o scot din stiva(din pachet)
            right_card_player2=deck.pop();//dau a doua carte playerului 2 si o scot din stiva(din pachet)
            left_card_player1=deck.pop();//dau a treia carte playerului 1 si o scot din stiva(din pachet)
            left_card_player2=deck.pop();//dau a patra carte playerului 2 si o scot din stiva(din pachet)
           
            try 
            {
                date=new ArrayList<>();
                date.add(right_card_player1);
                date.add(left_card_player1);
                date.add(right_card_player2);
                date.add(left_card_player2);
                if (nr==1)
                    date.add("Opponent starts first");
                else
                    date.add("I start first");
                player1_cards_sum=getCardValue(right_card_player1)+getCardValue(left_card_player1);
                player2_cards_sum=getCardValue(right_card_player2)+getCardValue(left_card_player2);
                date.add(((Integer)player1_cards_sum));//am trimis suma initiala a cartilor playerului 1
                date.add(((Integer)player2_cards_sum));//am trimis suma initiala a cartilor playerului 2
                
                player1_object_output_stream.writeObject(date);//am trimis primele doua carti playerului 1 si ale playerului2
                date.set(0,right_card_player2);
                date.set(1,left_card_player2);
                date.set(2,right_card_player1);
                date.set(3,left_card_player1);
                if (nr==1)
                    date.set(4,"I start first");
                else
                    date.set(4,"Opponent starts first");
                player2_cards_sum=getCardValue(right_card_player2)+getCardValue(left_card_player2);
                player1_cards_sum=getCardValue(right_card_player1)+getCardValue(left_card_player1);
                date.set(5,((Integer)player2_cards_sum));//am trimis suma initiala a cartilor playerului 2
                date.set(6,((Integer)player1_cards_sum));//am trimis suma initiala a cartilor playerului 1
                player2_object_output_stream.writeObject(date);//am trimis primele doua carti playerului 2 si primele doua ale playerului 1
                
                //aici se termina impartirea datelor si trimiterea datelor de inceput
       
                
                
                
                
                //playerii incep sa joace mana actuala
                if (nr==0)//player1 starts_first
                {
                    String comanda;
                    boolean player1_turn=true;
                    boolean player2_turn=false;
                     //astept mesajele de la playerul 2
                    wait_for_messages=new Thread(new WaitForMessages(player2_object_input_stream,player1_object_output_stream,Thread.currentThread(),this));
                    wait_for_messages.start();
                    while (player1_turn)
                    { 
                        date=new ArrayList<>();
                        synchronized(player1_object_input_stream)
                        {
                            date=(ArrayList<Object>)player1_object_input_stream.readObject();//astept o comanda de la player1
                        }
                        comanda=(String)date.get(0);
                        System.out.println("Am primit comanda: "+((String)date.get(0)));
                        if (comanda.equals("hit")==true)
                        {
                            date=new ArrayList<>();
                            date.add("carte");
                            String card;
                            card=deck.pop();
                            System.out.println("Cartea playerului 1: "+card);
                            date.add(card);
                            player1_cards_sum=player1_cards_sum+getCardValue(card);
                            date.add(((Integer)player1_cards_sum));//adaug suma cartilor
                            //nu pot sa ii trimit playerului 1 si cartea si mesajul de la playerul 2 asa ca trebuie sa sincronizez objectoutputstream-ul playerului 1 
                            synchronized(player1_object_output_stream)
                            {
                                player1_object_output_stream.writeObject(date);//ii trimit playerului 1 cartea playerului 1 doar cand nu primesc mesaj de la player 2
                            }
                            player2_object_output_stream.writeObject(date);//ii trimit playerului 2 cartea playerului 1
                            if(player1_cards_sum>=21)//daca suma cartilor e mai mare sau egala ca 21 s-a terminat randul meu
                            {
                                date=new ArrayList<>();
                                System.out.println("Suma e mai mare ca 21");
                                player1_turn=false;
                                player2_turn=true;
                                String my_turn="My turn";
                                date.add(my_turn);
                                player2_object_output_stream.writeObject(date);//ii zic playerului 2 ca e randul lui
                                 System.out.println("I-am zis playerului 2 ca e randul lui");
                            }
                        }
                        else
                        {
                            if(comanda.equals("stay"))
                            {
                                date=new ArrayList<>();
                                player1_turn=false;
                                player2_turn=true;
                                String my_turn="My turn";
                                date.add(my_turn);
                                player2_object_output_stream.writeObject(date);//ii zic playerului 2 ca e randul lui
                            }
                            if(comanda.equals("trimite mesaj"))
                            {
                                String mesaj_de_trimis;
                                mesaj_de_trimis = (String)date.get(1);
                                date=new ArrayList<>();
                                date.add("primeste mesaj");
                                date.add(mesaj_de_trimis);
                                player2_object_output_stream.writeObject(date);
                            }
                            if(comanda.equals("exit"))
                            {
                                System.out.println("Am primit de la playerul 1 o cerere de exit");
                                date=new ArrayList<>();
                                date.add("exit");
                                player2_object_output_stream.writeObject(date);//i-am zis playerului 2 ca playerul 1 a iesit din joc
                                Thread user_menu;
                                user_menu=new Thread(new Menu(this.player1_object_output_stream,this.player1_object_input_stream));
                                user_menu.start();
                                user_menu=new Thread(new Menu(this.player2_object_output_stream,this.player2_object_input_stream));
                                user_menu.start();
                                throw new Exception();
                            }
                        }
                    }
                    date=new ArrayList<>();
                    date.add("finish thread");//opresc threadul care asteapta mesaje al playerului 1
                    player1_object_output_stream.writeObject(date);
                    System.out.println("Playerul 1 nu mai asteapta mesajele de playerul 2");
                    
                    
                    
                    
                    //astept mesaje de la playerul 1
                    wait_for_messages=new Thread(new WaitForMessages(player1_object_input_stream,player2_object_output_stream,Thread.currentThread(),this));
                    wait_for_messages.start();
                    while(player2_turn)
                    {
                        System.out.println("A venit randul playerului 2"); 
                        date=new ArrayList<>();
                        synchronized(player2_object_input_stream)
                        {
                            date=(ArrayList<Object>)player2_object_input_stream.readObject();//astept o comanda de la player2
                        }
                        comanda=(String)date.get(0);
                        if (comanda.equals("hit")==true)
                        {
                            date=new ArrayList<>();
                            date.add("carte");
                            String card;
                            card=deck.pop();
                            date.add(card);
                            player2_cards_sum=player2_cards_sum+getCardValue(card);
                            date.add(((Integer)player2_cards_sum));
                            synchronized(player2_object_output_stream)
                            {
                                player2_object_output_stream.writeObject(date);//ii trimit playerului 2 cartea playerului 2 doar cand nu primesc mesaje de la playerul 1
                            }
                            player1_object_output_stream.writeObject(date);//ii trimit playerului 1 cartea playerului 2
                            if(player2_cards_sum>=21)
                            {
                                date=new ArrayList<>();
                                player2_turn=false;//s-a terminat tura playerului 2
                                String hand_has_ended="Hand has ended";
                                date.add(hand_has_ended);
                                player1_object_output_stream.writeObject(date);//ii zic playerului ca s-a terminat mana 
                            }
                        }
                        else
                        {
                            if(comanda.equals("stay"))
                            {
                                date=new ArrayList<>();
                                player2_turn=false;//s-a terminat tura playerului 2
                                String hand_has_ended="Hand has ended";
                                date.add(hand_has_ended);
                                player1_object_output_stream.writeObject(date);//ii zic playerului ca s-a terminat mana 
                            }
                           if(comanda.equals("trimite mesaj"))
                            {
                                String mesaj_de_trimis;
                                mesaj_de_trimis = (String)date.get(1);
                                date=new ArrayList<>();
                                date.add("primeste mesaj");
                                date.add(mesaj_de_trimis);
                                player1_object_output_stream.writeObject(date);
                            }
                            if(comanda.equals("exit"))
                            {
                                System.out.println("Am primit de la playerul 2 o cerere de exit");
                                date=new ArrayList<>();
                                date.add("exit");
                                player1_object_output_stream.writeObject(date);//i-am zis playerului 1 ca playerul 2 a iesit din joc
                                Thread user_menu;
                                user_menu=new Thread(new Menu(this.player2_object_output_stream,this.player2_object_input_stream));
                                user_menu.start();
                                user_menu=new Thread(new Menu(this.player1_object_output_stream,this.player1_object_input_stream));
                                user_menu.start();
                                throw new Exception();//am iesit din thread
                            }
                        }
                    }
                    date=new ArrayList<>();
                    date.add("finish thread");//opresc threadul care asteapta mesaje al playerului 2
                    player2_object_output_stream.writeObject(date);
                }
                
                
                
                if (nr==1)//player2 starts_first
                {
                    String comanda;
                    boolean player1_turn=false;
                    boolean player2_turn=true;
                    //astept mesaje de la playerul 1
                    wait_for_messages=new Thread(new WaitForMessages(player1_object_input_stream,player2_object_output_stream,Thread.currentThread(),this));
                    wait_for_messages.start();
                    while(player2_turn)
                    {    
                        date=new ArrayList<>();
                        synchronized(player2_object_input_stream)
                        {
                            date=(ArrayList<Object>)player2_object_input_stream.readObject();//astept o comanda de la player2
                        }
                        comanda=(String)date.get(0);
                        if (comanda.equals("hit")==true)
                        {
                            date=new ArrayList<>();
                            date.add("carte");
                            String card;
                            card=deck.pop();
                            date.add(card);
                            player2_cards_sum=player2_cards_sum+getCardValue(card);
                            date.add(((Integer)player2_cards_sum));
                            synchronized(player2_object_output_stream)
                            {
                                player2_object_output_stream.writeObject(date);//ii trimit playerului 2 cartea playerului 2 doar cand nu primesc mesaj de la playeerul 1
                            }
                            player1_object_output_stream.writeObject(date);//ii trimit playerului 1 cartea playerului 2
                            if(player2_cards_sum>=21)
                            {
                                date=new ArrayList<>();
                                player2_turn=false;
                                player1_turn=true;
                                String my_turn="My turn";
                                date.add(my_turn);
                                player1_object_output_stream.writeObject(date);//ii zic playerului 1 ca e randul lui sa joace
                            }
                        }
                        else
                        {
                            if(comanda.equals("stay"))
                            {
                                date=new ArrayList<>();
                                player2_turn=false;
                                player1_turn=true;
                                String my_turn="My turn";
                                date.add(my_turn);
                                player1_object_output_stream.writeObject(date);//ii zic playerului 1 ca e randul lui sa joace
                            }
                           if(comanda.equals("trimite mesaj"))
                            {
                                String mesaj_de_trimis;
                                mesaj_de_trimis = (String)date.get(1);
                                date=new ArrayList<>();
                                date.add("primeste mesaj");
                                date.add(mesaj_de_trimis);
                                player1_object_output_stream.writeObject(date);
                            }
                            if(comanda.equals("exit"))
                            {
                                System.out.println("Am primit de la playerul 2 o cerere de exit");
                                date=new ArrayList<>();
                                date.add("exit");
                                player1_object_output_stream.writeObject(date);//i-am zis playerului 1 ca playerul 2 a iesit din joc
                                Thread user_menu;
                                user_menu=new Thread(new Menu(this.player2_object_output_stream,this.player2_object_input_stream));
                                user_menu.start();
                                user_menu=new Thread(new Menu(this.player1_object_output_stream,this.player1_object_input_stream));
                                user_menu.start();
                                throw new Exception();//am iesit din thread
                            }
                        }
                    }
                    date=new ArrayList<>();
                    date.add("finish thread");//opresc threadul care asteapta mesaje al playerului 2
                    player2_object_output_stream.writeObject(date);
                    
                    
                    
                    
                    //astept mesajele de la playerul 2
                    wait_for_messages=new Thread(new WaitForMessages(player2_object_input_stream,player1_object_output_stream,Thread.currentThread(),this));
                    wait_for_messages.start();
                    while (player1_turn)
                    {    
                        date=new ArrayList<>();
                        synchronized(player1_object_input_stream)
                        {
                            date=(ArrayList<Object>)player1_object_input_stream.readObject();//astept o comanda de la player1
                        }
                        comanda=(String)date.get(0);
                        if (comanda.equals("hit")==true)
                        {
                            date=new ArrayList<>();
                            date.add("carte");
                            String card;
                            card=deck.pop();
                            date.add(card);
                            player1_cards_sum=player1_cards_sum+getCardValue(card);
                            date.add(((Integer)player1_cards_sum));//adaug suma cartilor
                            synchronized(player1_object_output_stream)
                            {
                                player1_object_output_stream.writeObject(date);//ii trimit playerului 1 cartea playerului 1
                            }
                            player2_object_output_stream.writeObject(date);//ii trimit playerului 2 cartea playerului 1 
                            if(player1_cards_sum>=21)
                            {
                                date=new ArrayList<>();
                                player1_turn=false;//s-a terminat tura playerului 1
                                String hand_has_ended="Hand has ended";
                                date.add(hand_has_ended);
                                player2_object_output_stream.writeObject(date);
                            }
                        }
                        else
                        {
                            if(comanda.equals("stay"))
                            {
                                date=new ArrayList<>();
                                player1_turn=false;//s-a terminat tura playerului 1
                                String hand_has_ended="Hand has ended";
                                date.add(hand_has_ended);
                                player2_object_output_stream.writeObject(date);
                            }
                            if(comanda.equals("trimite mesaj"))
                            {
                                String mesaj_de_trimis;
                                mesaj_de_trimis = (String)date.get(1);
                                date=new ArrayList<>();
                                date.add("primeste mesaj");
                                date.add(mesaj_de_trimis);
                                player2_object_output_stream.writeObject(date);
                            }
                            if(comanda.equals("exit"))
                            {
                                System.out.println("Am primit de la playerul 1 o cerere de exit");
                                date=new ArrayList<>();
                                date.add("exit");
                                player2_object_output_stream.writeObject(date);//i-am zis playerului 2 ca playerul 1 a iesit din joc
                                Thread user_menu;
                                user_menu=new Thread(new Menu(this.player1_object_output_stream,this.player1_object_input_stream));
                                user_menu.start();
                                user_menu=new Thread(new Menu(this.player2_object_output_stream,this.player2_object_input_stream));
                                user_menu.start();
                                throw new Exception();//am iesit din thread
                            }
                        }
                    }
                    date=new ArrayList<>();
                    date.add("finish thread");//opresc threadul care asteapta mesaje al playerului 1
                    player1_object_output_stream.writeObject(date);
                }
                //playerii termina de jucat mana
               
                if(nr==1)//jucatorul care a asteptat data trecuta va fi primul la aceasta mana
                    nr=0;
                else
                    nr=1;
            } 
            catch (IOException ex)
            {
                System.out.println("IOException"+ex.getMessage());
                break;
            } catch (ClassNotFoundException ex) {
                 Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                 break;
             }
            catch (Exception ex)
            {
                break;
            }
        }
    }
    public void shuffleCards()
    {
        cards=Server.cards;
        int i;
        for(i=0;i<cards.size()-2;i++)
        {
            int poz_random;
            String aux;
            poz_random = ThreadLocalRandom.current().nextInt(i+1, cards.size());
            aux=cards.get(i);
            cards.set(i,cards.get(poz_random));
            cards.set(poz_random,aux);
        }
        for (i=0;i<cards.size();i++)
            deck.add(cards.get(i));
    }
    public int getCardValue(String card)
    {
         switch(card)
        {
            case "2_inima-neagra":
                return 2;
            case "3_inima-neagra":
                return 3;
            case "4_inima-neagra":
                return 4;
            case "5_inima-neagra":
                return 5;
            case "6_inima-neagra":
                return 6;
            case "7_inima-neagra":
                return 7;
            case "8_inima-neagra":
                return 8;
            case "9_inima-neagra":
                return 9;
            case "10_inima-neagra":
                return 10;
            case "valet_inima-neagra":
                return 10;
            case "dama_inima-neagra":
                return 10;
            case "popa_inima-neagra":
                return 10;
            case "as_inima-neagra":
                return 1;

            case "2_inima-rosie":
                return 2;
            case "3_inima-rosie":
                return 3;
            case "4_inima-rosie":
                return 4;
            case "5_inima-rosie":
                return 5;
            case "6_inima-rosie":
                return 6;
            case "7_inima-rosie":
                return 7;
            case "8_inima-rosie":
                return 8;
            case "9_inima-rosie":
                return 9;
            case "10_inima-rosie":
                return 10;
            case "valet_inima-rosie":
                return 10;
            case "dama_inima-rosie":
                return 10;
            case "popa_inima-rosie":
                return 10;
            case "as_inima-rosie":
               return 1;

            case "2_romb":
                return 2;
            case "3_romb":
                return 3;
            case "4_romb":
                return 4;
            case "5_romb":
                return 5;
            case "6_romb":
                return 6;
            case "7_romb":
                return 7;
            case "8_romb":
                return 8;
            case "9_romb":
                return 9;
            case "10_romb":
                return 10;
            case "valet_romb":
                return 10;
            case "dama_romb":
                return 10;
            case "popa_romb":
                return 10;
            case "as_romb":
                return 1;

            case "2_trefla":
                return 2;
            case "3_trefla":
                return 3;
            case "4_trefla":
                return 4;
            case "5_trefla":
                return 5;
            case "6_trefla":
                return 6;
            case "7_trefla":
                return 7;
            case "8_trefla":
                return 8;
            case "9_trefla":
                return 9;
            case "10_trefla":
                return 10;
            case "valet_trefla":
                return 10;
            case "dama_trefla":
                return 10;
            case "popa_trefla":
                return 10;
            case "as_trefla":
                return 1;
            default :
                return 0;
        }
    }
}
