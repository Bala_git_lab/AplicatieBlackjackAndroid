/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bala
 */
public class WaitForMessages implements Runnable
{
    public ObjectInputStream player1objectinputstream;
    public ObjectOutputStream player2objectoutputstream;
    public ArrayList<Object> date;
    private String comanda;
    private String mesaj;
    private Thread game_thread;
    private Game game;
    public WaitForMessages(ObjectInputStream objectinputstream,ObjectOutputStream objectoutputstream,Thread game_thread,Game game)
    {
        this.player1objectinputstream=objectinputstream;
        this.player2objectoutputstream=objectoutputstream;
        date=new ArrayList<>();
        this.game=game;
        this.game_thread=game_thread;
    }
    @Override
    public void run()//astept mesajul de la un player si il trimit celuilalt player
    {
        try 
        {
            while(true)
            {
                date=new ArrayList<>();
                synchronized(player1objectinputstream)
                {
                    date=(ArrayList<Object>)player1objectinputstream.readObject();//serverul a primit mesajul de la player
                }
                comanda=(String)date.get(0);
                if(comanda.equals("mesaj"))
                {
                    synchronized(player2objectoutputstream)
                    {
                        player2objectoutputstream.writeObject(date);//serverul ii trimite mesajul celuilalt player doar atunci cand el nu vrea sa primeasca o carte
                    }
                }
                else
                {
                    if (comanda.equals("finish thread"))
                    {
                        System.out.println("s-a oprit threadul");
                        break;
                    }
                    if (comanda.equals("exit"))
                    {
                        synchronized(player2objectoutputstream)
                        {
                            player2objectoutputstream.writeObject(date);//serverul ii trimite mesajul celuilalt player doar atunci cand el nu vrea sa primeasca o carte
                        }
                        game_thread.interrupt();//am oprit threadul cu jocul
                        Thread user_menu;//meniuri pt
                        user_menu=new Thread(new Menu(game.player1_object_output_stream,game.player1_object_input_stream));
                        user_menu.start();
                        user_menu=new Thread(new Menu(game.player2_object_output_stream,game.player2_object_input_stream));
                        user_menu.start();
                        break;
                    }
                    break;
                }
            }
        }
        catch (IOException ex)
        {
            Logger.getLogger(WaitForMessages.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (ClassNotFoundException ex) 
        {
            Logger.getLogger(WaitForMessages.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
    
}
